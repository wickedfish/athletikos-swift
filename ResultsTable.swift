//
//  ResultsTable.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/6/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class ResultsTable: UITableViewController {
    
    var userJustCompletedWorkout = false
    
    //This array holds the workout results
    var completedWorkout = Workout()
    
    let statusImages = [UIImage(named: "X.png"), UIImage(named: "check.png")]
    
    @IBOutlet weak var timeButtonItem : UIBarButtonItem!
    
    //Currently not using this.
    @IBAction func returnToPersonal(_ segue: UIStoryboardSegue) {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatTime()
        
        self.tableView.allowsSelection = false //These cells don't do anything, so they shouldn't be select-able.
        
        if userJustCompletedWorkout {
            //Customize the back button so that it goes back to a root page.
            self.navigationItem.hidesBackButton = true
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back",
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(backButtonDidTouch))
        }
        
        
        self.navigationItem.title = "Results"

    }
    
    //Correctly formats the time elapsed, to be displayed in a bar button.
    func formatTime() {
        let timeElapsed = completedWorkout.time_elapsed
        let minutes = timeElapsed / 60
        let seconds = timeElapsed % 60
        if timeElapsed >= 60 {
            
            if seconds < 10 {
                if minutes < 10 {
                    timeButtonItem.title = "0\(minutes):0\(seconds)"
                }
                else {
                    timeButtonItem.title = "\(minutes):0\(seconds)"
                }
            } else {
                if minutes < 10 {
                    timeButtonItem.title = "0\(minutes):\(seconds)"
                }
                else {
                    timeButtonItem.title = "\(minutes):\(seconds)"
                }
            }
        }
        else  {//If it's less than 60 seconds
            if timeElapsed < 10 {
                timeButtonItem.title = "00:0\(timeElapsed)"
            } else {
                timeButtonItem.title = "00:\(timeElapsed)"
            }
        }
        
    }

    
    
    func backButtonDidTouch() {
        performSegue(withIdentifier: "toPersonalPage", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return completedWorkout.exercises.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ResultsCell
        
        cell.exerciseNameLabel.text = completedWorkout.exercises[indexPath.row].exercise_name

        if completedWorkout.exercises[indexPath.row].exercise_completed {
            cell.checkXIcon.image = statusImages[1]
        }
        else {
            cell.checkXIcon.image = statusImages[0]
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true) //Makes things pretty
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWorkoutResults" {
            //let dest = segue.destination as! PersonalViewController
            
            
            //Might put something here.
    
        }
    }
}
