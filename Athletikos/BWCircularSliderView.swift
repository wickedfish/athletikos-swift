//
//  BWCircularSliderView.swift
//  TB_CustomControlsSwift
//
//  Created by Yari D'areglia on 14/11/14.
//  Copyright (c) 2014 Yari D'areglia. All rights reserved.
//

import UIKit

@IBDesignable class BWCircularSliderView: UIView {
    
    var currentValue : Int = 0 //Used by the workoutVC to access the value of the dial.
    var userDidChangeValue : Bool = false //Used by the WorkoutVC to know if a rep or weight value has been modified.
    //var isRepsDial : Bool = false
    var isRepsDial : Bool = false
    
    let stringle = Stringleton.sharedInstance
    
    @IBOutlet weak var controller: TapExDetailsVC!
    
    @IBInspectable var repsAngle = 0
    @IBInspectable var weightAngle = 0
    
    @IBInspectable var frangle = 0
    
    @IBInspectable var startColor:UIColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    @IBInspectable var endColor:UIColor = UIColor(red:0.25, green:0.60, blue:1.00, alpha:1.0)
    
    #if TARGET_INTERFACE_BUILDER
    override func willMoveToSuperview(newSuperview: UIView?) {
    
    if stringle.isDialOne!{
        frangle = stringle.reps*12
        stringle.isDialOne = false
    
        let slider:BWCircularSlider = BWCircularSlider(startColor:self.startColor, endColor:self.endColor, frame: self.bounds, angle: frangle, value: stringle.reps)
    
    }else{
        frangle = Int(Double(stringle.weight)/1.4)
    
        let slider:BWCircularSlider = BWCircularSlider(startColor:self.startColor, endColor:self.endColor, frame: self.bounds, angle: frangle, value: stringle.weight)
    
    }
        self.addSubview(slider)
    }
    
    #else
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        if stringle.isDialOne!{
            frangle = stringle.reps*12
            stringle.isDialOne = false
            
            let slider:BWCircularSlider = BWCircularSlider(startColor:self.startColor, endColor:self.endColor, frame: self.bounds, frangle: frangle, value: stringle.reps)
            
            // Attach an Action and a Target to the slider
            slider.addTarget(self, action: #selector(BWCircularSliderView.valueChanged(_:)), for: UIControlEvents.valueChanged)
            
            // Add the slider as subview of this view
            self.addSubview(slider)
            slider.backgroundColor = UIColor.clear
            
        }else{
            frangle = Int(Double(stringle.weight)/1.4)
            
            let slider:BWCircularSlider = BWCircularSlider(startColor:self.startColor, endColor:self.endColor, frame: self.bounds, frangle: frangle, value: stringle.weight)
            
            // Attach an Action and a Target to the slider
            slider.addTarget(self, action: #selector(BWCircularSliderView.valueChanged(_:)), for: UIControlEvents.valueChanged)
            
            // Add the slider as subview of this view
            self.addSubview(slider)
            slider.backgroundColor = UIColor.clear
        }
        
        self.backgroundColor = UIColor.clear
        
    }
    #endif
    
    func valueChanged(_ slider:BWCircularSlider){

        //Speedy's tweaks: I added an if-else so that reps dial can increment by 1.
        if isRepsDial {
            repsAngle = slider.angle/12
            currentValue = repsAngle
            
            userDidChangeValue = true
            
            slider.textField.text = String(repsAngle)
        }
        else {
            // Do something with the value...
            weightAngle = Int(1.4 * Double(slider.angle))
            weightAngle = weightAngle - weightAngle%5
            currentValue = weightAngle
        
            userDidChangeValue = true
        
            slider.textField.text = String(weightAngle)
        }
    }
}
