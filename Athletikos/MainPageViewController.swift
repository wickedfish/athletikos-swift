//
//  MainPageViewController.swift
//  Athletikos
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//inweak var tutorialDelegate: TutorialPageViewControllerDelegate?


import UIKit
import UserNotifications


class MainPageViewController: UIPageViewController {

    weak var mainPageDelegate: MainPageViewControllerDelegate?
    let userData = Fireton.sharedInstance
    var defaults = UserDefaults.standard
    
    fileprivate(set) lazy var MainPageViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [self.newViewController("Personal"),
                     self.newViewController("Subscribed"),
                     self.newViewController("Coached")]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        delegate = self
        
        //loads initial page according to main page settings in user defaults
        if(UserDefaults.standard.object(forKey: "homePage") == nil){
            if let initialViewController = MainPageViewControllers.first {
                scrollToViewController(initialViewController)
            }
        } else {
            switch(UserDefaults.standard.object(forKey: "homePage") as! String){
                case "My Page":
                    if let initialViewController = MainPageViewControllers.first {
                        scrollToViewController(initialViewController)
                    }
                case "My Teams":
                        scrollToViewController(MainPageViewControllers[1])
                case  "Coached Teams":
                        scrollToViewController(MainPageViewControllers[2])
                default:
                        UserDefaults.standard.set("My Page", forKey: "homePage")
                        if let initialViewController = MainPageViewControllers.first {
                            scrollToViewController(initialViewController)
                }
            }
        }
        mainPageDelegate?.MainPageViewController(self,didUpdatePageCount: MainPageViewControllers.count)
        
        
        //schedules usernotification depending on notification settings
        //nonotification sceduled if notification settings set to none
        let notifSettings = defaults.object(forKey: "notifications")
        
        var notifSettingString: String!
        
        if(notifSettings != nil){
            notifSettingString = notifSettings as! String
        } else {
            notifSettingString = "all"
        }
        
        if( notifSettings == nil || (notifSettings as! String) != "none"){
            //schedules team notifications
            if(notifSettings == nil || (notifSettings as! String) == "all" || (notifSettings as! String) == "team"){
                for team in userData.teams{
                    for workout in team.workouts{
                    
                        //create notification
                        let note = UNMutableNotificationContent()
                        note.title = "Workout Reminder"
                        note.categoryIdentifier = "testCat"
                        note.body = "You have a workout scheduled today for \(team.team_name)"
                        note.sound = UNNotificationSound.default()
                    
                    
                        var dateComponents = DateComponents()
                        dateComponents.day = workout.day
                        dateComponents.month = workout.month
                        dateComponents.year = workout.year
                        dateComponents.calendar = Calendar(identifier: .gregorian)
                        dateComponents.hour = 8
                        dateComponents.minute = 0
                        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                        let request = UNNotificationRequest(identifier: "testCat", content: note, trigger: trigger)
                    
                    
                        let center = UNUserNotificationCenter.current()
                        center.add(request, withCompletionHandler: {
                            (error) in
                            if error != nil {
                                print("notfication error")
                            }
                        })
                    }
                }
            }
            //schedules personal notifications
            if(notifSettings == nil || notifSettingString == "all" || notifSettingString == "personal"){
                for personalWorkout in userData.personalWorkouts{
                    
                    //create notification
                    let note = UNMutableNotificationContent()
                    note.title = "Workout Reminder"
                    note.categoryIdentifier = "testCat"
                    note.body = "You have a workout scheduled today."
                    note.sound = UNNotificationSound.default()
                    
                    var dateComponents = DateComponents()
                    dateComponents.day = personalWorkout.day
                    dateComponents.month = personalWorkout.month
                    dateComponents.year = personalWorkout.year
                    dateComponents.calendar = Calendar(identifier: .gregorian)
                    dateComponents.hour = 8
                    dateComponents.minute = 0
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                    let request = UNNotificationRequest(identifier: "testCat", content: note, trigger: trigger)
                    
                    let center = UNUserNotificationCenter.current()
                    center.add(request, withCompletionHandler: {
                        (error) in
                        if error != nil {
                            print("notfication error")
                        }
                    })
                }
            }
        } else {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    fileprivate func newViewController(_ page: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
        instantiateViewController(withIdentifier: "\(page)Page")
    }
    
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self,
                                                        viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController)
        }
    }
    
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = MainPageViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = MainPageViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    fileprivate func scrollToViewController(_ viewController: UIViewController,direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            // 'tutorialDelegate' of the new index.
                            self.notifyTutorialDelegateOfNewIndex()
        })
    }
    
    fileprivate func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = MainPageViewControllers.index(of: firstViewController) {
            mainPageDelegate?.MainPageViewController(self,
                                                         didUpdatePageIndex: index)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = MainPageViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return MainPageViewControllers.last
        }
        
        guard MainPageViewControllers.count > previousIndex else {
            return nil
        }
        
        return MainPageViewControllers[previousIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = MainPageViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let MainPageViewControllersCount = MainPageViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard MainPageViewControllersCount != nextIndex else {
            return MainPageViewControllers.first
        }
        
        guard MainPageViewControllersCount > nextIndex else {
            return nil
        }
        
        return MainPageViewControllers[nextIndex]
    }
}

extension MainPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        if let firstViewController = viewControllers?.first,
            let arrayIndex = MainPageViewControllers.index(of: firstViewController) {
            switch arrayIndex {
            case 0:
                self.navigationController!.navigationBar.topItem!.title = "My Page"
                break
                
            case 1:
                self.navigationController!.navigationBar.topItem!.title = "My Teams"
                break
                
            case 2:
                self.navigationController!.navigationBar.topItem!.title = "Coached Teams"
                break

            default:
                self.navigationController!.navigationBar.topItem!.title = "Error"
                
                
            }
        
        notifyTutorialDelegateOfNewIndex()
        }}
    }


protocol MainPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter count: the total number of pages.
     */
    func MainPageViewController(_ MainPageViewController: MainPageViewController,
                                    didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func MainPageViewController(_ MainPageViewController: MainPageViewController,
                                didUpdatePageIndex index: Int)
    
}


