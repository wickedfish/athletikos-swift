//
//  WorkoutCell.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/6/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class WorkoutCell: UITableViewCell {
    
    @IBOutlet weak var exercise_name : UILabel!
    @IBOutlet weak var sets : UILabel!
    @IBOutlet weak var reps : UILabel!
    @IBOutlet weak var weight : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
