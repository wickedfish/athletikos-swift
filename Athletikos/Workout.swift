//
//  Workout.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/27/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation

struct Workout {
    var workout_id : String
    var workout_name : String
    var owner : String
    var workout_type : String
    
    //Date variables can be used for due date or date completed
    var day : Int
    var month : Int
    var year : Int
    
    var day_incrementor : Int
    
    var workout_completed : Bool
    
    var time_elapsed : Int //Only relevant for completed workouts. The time elapsed in seconds.
    
    var exercises = [Exercise]()
    
    init() {
        self.workout_id = "null"
        self.workout_name = "null"
        self.owner = "null"
        self.day = 0
        self.month = 0
        self.year = 0
        self.day_incrementor = 0
        self.workout_completed = false
        self.workout_type = "null"
        self.time_elapsed = 0
    }
}
