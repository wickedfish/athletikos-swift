//
//  profileSetupVC.swift
//  Athletikos
//
//  Created by Daniel Porter on 3/29/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import RevealingSplashView
import EZLoadingActivity

class profileSetupVC: UIViewController {

    //Who called this view
    var sentFrom : String!
    let fire = Fireton.sharedInstance
    let defaults = UserDefaults.standard
    var alreadyLaidout = false
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: style.logoName)!,iconInitialSize: CGSize(width: 100, height: 100), backgroundColor: style.blue)
    
    let lightBlue = UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1)
    
    @IBOutlet weak var passwordResetButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var usernameTextField: JVFloatLabeledTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileSetupVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        cancelButton.setTitleColor(style.white, for: .normal)
        passwordResetButton.setTitleColor(style.white, for: .normal)
        submitButton.setTitleColor(UIColor .lightText, for: .disabled)
        submitButton.setTitleColor(style.white, for: .normal)
        
        view.backgroundColor = style.blue
        
        if sentFrom == "login" {
            cancelButton.isHidden = true
            submitButton.isEnabled = true
            passwordResetButton.isHidden = true
            self.view.addSubview(revealingSplashView)
            revealingSplashView.startAnimation(){
                print("Completed")
            }
        } else if sentFrom == "settings" {
            submitButton.isEnabled = true
            cancelButton.isHidden = false
            usernameTextField.text = FIRAuth.auth()?.currentUser?.displayName!
            navigationController?.isNavigationBarHidden = true
        } else if sentFrom == "loginWOSplash" {
            cancelButton.isHidden = true
            passwordResetButton.isHidden = true
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        if(!alreadyLaidout) {
            let bottomLineEMAIL = CALayer()
            bottomLineEMAIL.frame = CGRect(x: 0.0, y: usernameTextField.frame.height + 1, width: usernameTextField.frame.width, height: 2.0)
            bottomLineEMAIL.backgroundColor = style.white.cgColor
            usernameTextField.borderStyle = .none
            usernameTextField.layer.addSublayer(bottomLineEMAIL)
            usernameTextField.textColor = style.white
            usernameTextField.floatingLabelTextColor = style.white
            usernameTextField.floatingLabelActiveTextColor = style.white
            usernameTextField.floatingLabelFont = usernameTextField.floatingLabelFont.withSize(13)
            
            alreadyLaidout = true
        }
    }

    @IBAction func submitPressed(_ sender: Any) {
        if usernameTextField.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter username", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
        }
        else if let user = FIRAuth.auth()?.currentUser {
            EZLoadingActivity.show("Updating...", disableUI: true)
            let changeRequest = user.profileChangeRequest()
            changeRequest.displayName = self.usernameTextField.text
            changeRequest.commitChanges { error in
                if let error = error {
                    EZLoadingActivity.hide(false, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        print(error.localizedDescription)
                    })
                } else {
                    EZLoadingActivity.hide(true, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        if(self.sentFrom == "settings") {
                            self.performSegue(withIdentifier: "backToSettings", sender: self)
                        } else if (self.sentFrom == "login" || self.sentFrom == "loginWOSplash") {
                            self.performSegue(withIdentifier: "loadFromInitialSetup", sender: self)
                        }
                    })
                }
            }
        } else {
            //This shouldn't run...
            EZLoadingActivity.show("Updating...", disableUI: true)
            EZLoadingActivity.hide(false, animated: true)
            print("BAD BAD BAD")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func passwordResetPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Password Reset", message: "Send Password Rest Email?", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title:"Yes", style: .cancel, handler:  { action in FIRAuth.auth()?.sendPasswordReset(withEmail: (FIRAuth.auth()?.currentUser?.email)!, completion: nil)}))
        
        alertController.addAction(UIAlertAction(title:"No", style: .destructive, handler:nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
