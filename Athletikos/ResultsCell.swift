//
//  ResultsCell.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/6/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class ResultsCell: UITableViewCell {
    
    @IBOutlet weak var checkXIcon : UIImageView!
    @IBOutlet weak var exerciseNameLabel : UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
