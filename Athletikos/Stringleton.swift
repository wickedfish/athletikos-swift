//
//  Stringleton.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 3/1/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation

class Stringleton: NSObject {
    
    let userRefRoot = "athletikos_data/users/"
    let workoutRefPath = "athletikos_data/workouts"
    let exerciseRefPath = "athletikos_data/exercises"
    let preMadeRefPath = "athletikos_data/pre_made_workout"
    let teamRefPath = "athletikos_data/teams"
    
    let coached_teams = "coached_teams"
    let subscribed_teams = "subscribed_teams"
    let display_name = "display_name"
    let completed_exercises = "completed_exercises"
    let day_incrementor = "day_incrementor"
    let workout_completed = "workout_completed"
    let date_completed = "date_completed"
    let day = "day"
    let month = "month"
    let year = "year"
    let exercises = "exercises"
    let created_exercises = "created_exercises"
    let data_tracking = "data_tracking"
    let goals = "goals"
    let personal_workouts = "personal_workouts"
    let date = "date"
    let workout_id = "workout_id"
    let owner = "owner"
    let workouts = "workouts"
    let workout_name = "workout_name"
    let workout_type = "workout_type"
    let coach_id = "coach_id"
    let completed_workouts = "completed_workouts"
    let team_members = "team_members"
    let team_name = "team_name"
    
    let firetonFindWorkoutError = "Fireton Error: workout ID not found"
    let firetonKeyError = "Fireton: Unrecognized key"
    let firetonKeyError2 = "Fireton: Unrecognized key2"
    
    var reps: Int!
    var weight: Int!
    var isDialOne: Bool!

    
    //Make this a Singleton class
    static let sharedInstance: Stringleton = {
        let instance = Stringleton()
        return instance
    }()
    
}
