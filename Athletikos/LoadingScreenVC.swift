//
//  LoadingScreenVC.swift
//  Athletikos
//
//  Created by Daniel Porter on 3/20/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import RevealingSplashView

class LoadingScreenVC: UIViewController {

    let fire = Fireton.sharedInstance
    var loadingFinish = false
    var viewFinish = false
    var observerAdded = false
    var alreadyRan = false
    var sentFrom: String? = nil
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: style.logoName)!,iconInitialSize: CGSize(width: 100, height: 100), backgroundColor: style.blue)
    

    @IBOutlet weak var loadingLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        alreadyRan = false
        
        if let sentFrom = sentFrom {
            if sentFrom == "login" {
                self.view.addSubview(revealingSplashView)
            }
        } else {
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //fire.removeObserver(self, forKeyPath: "hasFinishedDownloading")
        print("Loading Observer Ran")
        if let sentFrom = sentFrom {
            if sentFrom == "login" {
                revealingSplashView.startAnimation(){
                    self.performSegue(withIdentifier: "ToInitialScreens", sender: nil)
                }
            }
        } else {
            self.performSegue(withIdentifier: "ToInitialScreens", sender: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !alreadyRan {
            alreadyRan = true
            if fire.hasFinishedDownloading {
                print("Already Loaded")
                if let sentFrom = sentFrom {
                    if sentFrom == "login" {
                        revealingSplashView.startAnimation(){
                            self.performSegue(withIdentifier: "ToInitialScreens", sender: nil)
                        }
                    }
                } else {
                    self.performSegue(withIdentifier: "ToInitialScreens", sender: nil)
                }
            } else {
                print("Not Loaded")
                observerAdded = true
                fire.addObserver(self, forKeyPath: "hasFinishedDownloading", options: .new, context: nil)
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        if(observerAdded) {
            fire.removeObserver(self, forKeyPath: "hasFinishedDownloading")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
