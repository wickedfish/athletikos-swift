//
//  WorkoutCompletionObject.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 4/1/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import Firebase

struct WorkoutCompletionObject {
    
    //Each of these month arrays has workout data in [dayOfMonth : NumberOfWorkoutsCompleted] format
    
    var monthData = [String : Int]()
    
    var monthThatDataIsFor : String
    
    var total_workouts : Int
    
    var week = [String : Int]()
    
    var weekStartDay : Int
    var weekStartMonth : Int
    var weekStartYear : Int
    
    init() {
        weekStartDay = 0
        weekStartMonth = 0
        weekStartYear = 0
        total_workouts = 0
        monthThatDataIsFor = "null"
    }
    
    init(snapshot: FIRDataSnapshot) {
        
        weekStartDay = 0
        weekStartMonth = 0
        weekStartYear = 0
        total_workouts = 0
        monthThatDataIsFor = "null"
        
        for completionBranch in snapshot.children {
            let branchSnap = completionBranch as! FIRDataSnapshot
            if "\(branchSnap.key)" == "January" || "\(branchSnap.key)" == "February" || "\(branchSnap.key)" == "March" || "\(branchSnap.key)" == "April" || "\(branchSnap.key)" == "May" || "\(branchSnap.key)" == "June" || "\(branchSnap.key)" == "July" || "\(branchSnap.key)" == "August" || "\(branchSnap.key)" == "September" || "\(branchSnap.key)" == "October" || "\(branchSnap.key)" == "November" || "\(branchSnap.key)" == "December" {
                monthThatDataIsFor = "\(branchSnap.key)"
                for day in branchSnap.children {
                    let daySnap = day as! FIRDataSnapshot
                    
                    monthData["\(daySnap.key)"] = daySnap.value as! Int?
                }
            }
            else if "\(branchSnap.key)" == "total_workouts" {
                total_workouts = branchSnap.value as! Int
            }
            else if "\(branchSnap.key)" == "week" {
                for weekDay in branchSnap.children {
                    let weekDaySnap = weekDay as! FIRDataSnapshot
                    
                    if "\(weekDaySnap.key)" == "start_date" {
                        for dateItem in weekDaySnap.children {
                            
                            let dateSnap = dateItem as! FIRDataSnapshot
                            if "\(dateSnap.key)" == "day" {
                                weekStartDay = dateSnap.value as! Int
                            }
                            else if "\(dateSnap.key)" == "month" {
                                weekStartMonth = dateSnap.value as! Int
                            }
                            else if "\(dateSnap.key)" == "year" {
                                weekStartYear = dateSnap.value as! Int
                            }
                            else {
                                print("WorkoutCompletionObject Error: ")
                                print("Firebase Init: Date")
                            }
                            
                        }

                    }
                    else {
                        week["\(weekDaySnap.key)"] = weekDaySnap.value as! Int?
                    }
                }
            }
            else {
                print("WorkoutCompletionObject Error: ")
                print("Firebase Init: Key not recognized")
                print("\(branchSnap.key)")
            }
            
        }
    }
}
