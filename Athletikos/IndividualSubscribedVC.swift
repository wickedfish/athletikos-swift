//
//  IndividualSubscribedVC.swift
//  Athletikos
//
//  Created by Joshua Blasdell on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Charts
import Firebase
import FirebaseAuth

private let reuseIdentifier = "SubscribedGoalCell"

class IndividualSubscribedVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,UICollectionViewDelegateFlowLayout, ChartViewDelegate {
    
    let fire = Fireton.sharedInstance
    var row = 0
    var teamID = ""
    var workoutName = ""
    var teamName = ""
    var currentWorkout: Workout!
    var dayCompleted = [String : Int]()
    var workoutsCompleted = [Int]()
    var dayCompletedWeekly = [String : Int]()
    var workoutsCompletedWeekly = [Int]()
    let days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    let week = ["","S","M","T","W","T","F","S"]
    var descriptionText: String!
    var loadMonth = true
    var format : IAxisValueFormatter!
    var tempName = ""
    var i = 0
    
    var teamRoutines = [(String , Date, DateComponents)]()
    
    
    @IBOutlet weak var subscribedGoalsCollection: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var chartSegControl: UISegmentedControl!

    @IBAction func chartSegControlChange(){
        if(chartSegControl.selectedSegmentIndex == 0){
            loadMonthChart()
            loadMonth = false
        } else{
            loadWeekChart()
            loadMonth = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = style.blue

        tableView.layer.borderWidth = 3
        tableView.layer.borderColor = style.blue.cgColor
        tableView.layer.cornerRadius = 10
        subscribedGoalsCollection.layer.borderWidth = 3
        subscribedGoalsCollection.layer.borderColor = style.blue.cgColor
        subscribedGoalsCollection.layer.cornerRadius = 10
        
        self.title = teamName
        
        dayCompleted = fire.findTeam(withID: teamID).workoutCompletionData.monthData
        dayCompletedWeekly = fire.findTeam(withID: teamID).workoutCompletionData.week
        
        
        for i in 1 ..< 31 {
            if(dayCompleted[String(i)] != nil){
                workoutsCompleted.append(dayCompleted[String(i)]!)
            }
        }
        
        for i in 1 ..< week.count+1 {
            if(dayCompletedWeekly[String(i)] != nil){
                workoutsCompletedWeekly.append(dayCompletedWeekly[String(i)]!)
            }
        }
        
        //loads chart
        loadMonthChart()
        loadMonth = false
        
        format = self.lineChartView.xAxis.valueFormatter
        
        //creates array of workout names and dates for the workout then sorts based on date
        loadWorkoutData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadWorkoutData(){
        for workout in fire.findTeam(withID: teamID).workouts{
            for routine in fire.workouts{
                if routine.workout_id == workout.workout_id{
                    var workoutDate = DateComponents()
                    workoutDate.day = workout.day
                    workoutDate.month = workout.month
                    workoutDate.year = workout.year
            
                    let cal = Calendar.current
            
                    teamRoutines.append((routine.workout_name, cal.date(from: workoutDate)!,workoutDate))
                }
            }
        }
        /*
        for (userID, thisworkout) in (fire.findTeam(withID: teamID).allCompletedWorkouts[workoutName]?.workoutPerUser)!{
            if userID == FIRAuth.auth()?.currentUser?.uid {
                for _ in teamRoutines{
                    if thisworkout.workout_name == teamRoutines[i].0{
                        teamRoutines.remove(at: i)
                        i += 1
                    }else{
                        i += 1
                    }
                    
                }
            }
        }*/
        
        teamRoutines.sort{$0.1 < $1.1}
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for team in fire.teams{
            if team.team_id == teamID{
                if(team.workouts.count > 4){
                    return 4
                }else{
                    return team.workouts.count
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nextCell", for: indexPath) as! PersonalWorkoutCell
        cell.workoutLabel.text = teamRoutines[(indexPath as NSIndexPath).row].0
        var workoutDateComponents = teamRoutines[(indexPath as NSIndexPath).row].2
        
        let dateFormatter: DateFormatter = DateFormatter()
        let months = dateFormatter.shortMonthSymbols
        let monthSymbol = months?[workoutDateComponents.month!-1]
        let cal = Calendar.current
        
        if(cal.isDateInToday(teamRoutines[(indexPath as NSIndexPath).row].1) ){
            cell.dateLabel.text = "Today"
        }else {
            cell.dateLabel.text = "\(monthSymbol!) \(workoutDateComponents.day!)"
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row
        
        return indexPath
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        for team in fire.teams{
            if team.team_id == teamID{
                if(team.goals.count == 1){
                    let totalCellWidth =  169 * team.goals.count
                    let totalSpacingWidth = 10 * (team.goals.count - 1)
            
                    let leftInset = ((self.subscribedGoalsCollection.frame.width/2) - CGFloat(totalCellWidth + totalSpacingWidth) / 2)
                    let rightInset = leftInset
            
                    return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
                } else if(team.goals.count == 2){
                    let totalCellWidth =  169 * team.goals.count
                    let totalSpacingWidth = 10 * (team.goals.count - 1)
            
                    let leftInset = ((self.subscribedGoalsCollection.frame.width/2) - CGFloat(Int(totalCellWidth) + totalSpacingWidth)/2) 
                    let rightInset = leftInset
            
                    return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
                }
            }
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        for team in fire.teams{
            if team.team_id == teamID{
                if(team.goals.count > 4){
                    return 4
                }else{
                    return team.goals.count
                }
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GoalCollectionCell
        for team in fire.teams{
            if team.team_id == teamID{
                let currentValue = team.goals[(indexPath as NSIndexPath).row].current_val
                let target = team.goals[(indexPath as NSIndexPath).row].target_val
                
                cell.goalLabel.text = team.goals[(indexPath as NSIndexPath).row].tracker_id
                
                if(target > currentValue){
                    cell.showProgress(goal: Double(target), progress: Double(currentValue))
                } else {
                    cell.showProgress(goal: Double(target), progress: Double(1.0))
                }
            }
        }
        return cell
    }
    
    func setChartData(day : [Int]) {
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        
        if(loadMonth){
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< 31 {
                if(dayCompleted.keys.contains(String(i))){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(workoutsCompleted.prefix(yVals1.count).reduce(0, +))))
                }
            }
        } else{
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< week.count+1 {
                if(dayCompletedWeekly.keys.contains(String(i))){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(workoutsCompletedWeekly.prefix(yVals1.count).reduce(0, +))))
                }
            }
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "First Set")
        set1.axisDependency = .left // Line will correlate with left axis values
        set1.setColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 2.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)
        set1.highlightColor = UIColor.white
        set1.drawCircleHoleEnabled = false
        
        //3 - create an array to store our LineChartDataSets
        var dataSets = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData!
        if(loadMonth){
            data = LineChartData(dataSets: dataSets)
            data.setValueTextColor(UIColor.white)
        } else{
            data = LineChartData(dataSets: dataSets)
            data.setValueTextColor(UIColor.white)
        }
        
        //5 - finally set our data
        self.lineChartView.data = data
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInProgressFromSubscribedTeams" {
            
            let dest = segue.destination as! WorkoutVC
            dest.isTeamWorkout = true
            dest.IDOfTeamThatSentTheWorkout = teamID
            let workoutDateComponents = teamRoutines[row].2 //the .2 means the third (0,1,2) item in this wierd data structure.
            dest.dateComponentsOfWorkout = workoutDateComponents
            let team = fire.findTeam(withID: teamID)
            dest.workout_id = team.workouts[row].workout_id  //*** SET this to the relevant workoutID
            
        }
    }
    
    //loads chart
    func loadMonthChart() {
        //if true loads month data
        self.lineChartView.delegate = self
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 6
        self.lineChartView.leftAxis.axisMaximum = 30
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 6
        self.lineChartView.xAxis.axisMaximum = 30
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.drawBordersEnabled = true
        self.lineChartView.xAxis.valueFormatter = format
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        self.lineChartView.highlightPerTapEnabled = false
        self.lineChartView.doubleTapToZoomEnabled = false
        
        setChartData(day: days)
    }
    
    func loadWeekChart() {
        self.lineChartView.delegate = self
        //self.lineChartView.chartDescription?.text = "Tap node for details"
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 7
        self.lineChartView.leftAxis.axisMaximum = 7
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 7
        self.lineChartView.xAxis.axisMaximum = 7
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:week)
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        
        setChartData(day: days)
    }

}
