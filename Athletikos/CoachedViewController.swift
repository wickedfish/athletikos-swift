//
//  CoachedViewController.swift
//  Athletikos
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import UserNotifications

class CoachedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    let userData = Fireton.sharedInstance
    var row = 0
    let style = Styleton.sharedInstance
    var iPath = IndexPath()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        view.backgroundColor = style.blue
        tableView.layer.cornerRadius = 10
        tableView.layer.borderWidth = 3
        tableView.layer.borderColor = style.blue.cgColor
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.coachedTeams.count // your number of cell here
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! CoachedTeamCell
        for team in userData.teams{
            if team.team_id == userData.coachedTeams[(indexPath as NSIndexPath).row]{
                cell.coachedTeamLabel.text = team.team_name
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        row = (indexPath as NSIndexPath).row
        iPath = indexPath
        return indexPath
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     let dest = (segue.destination as! IndividualCoachedVC)
        dest.teamID = userData.coachedTeams[row]
        dest.teamName = (tableView.cellForRow(at: iPath) as! CoachedTeamCell).coachedTeamLabel.text!
    }
}
