//
//  PersonalWorkoutCellTableViewCell.swift
//  Athletikos
//
//  Created by Andrew Henry on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class PersonalWorkoutCell: UITableViewCell {

    @IBOutlet weak var workoutLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
