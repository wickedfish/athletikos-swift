//
//  CoachedTeamCellTableViewCell.swift
//  Athletikos
//
//  Created by Andrew Henry on 2/4/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class CoachedTeamCell: UITableViewCell {

    @IBOutlet weak var coachedTeamLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
