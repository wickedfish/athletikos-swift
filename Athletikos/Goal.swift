//
//  Goal.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/28/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import Firebase

struct Goal {
    var current_val : Int
    var start_val : Int
    var target_val : Int
    var tracker_id : String
    var exercise_name : String
    var goal_type : String
    var goal_key : String
    
    init(current_val: Int, start_val : Int,
    target_val : Int,
    tracker_id : String, exercise_name: String, goal_type: String) {
        self.current_val = current_val
        self.start_val = start_val
        self.target_val = target_val
        self.tracker_id = tracker_id
        self.exercise_name = exercise_name
        self.goal_type = goal_type
        goal_key = "null"
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        goal_key = snapshot.key
        current_val = snapshotValue["current_val"] as! Int
        start_val = snapshotValue["start_val"] as! Int
        target_val = snapshotValue["target_val"] as! Int
        tracker_id = snapshotValue["tracker_id"] as! String
        exercise_name = snapshotValue["exercise"] as! String
        goal_type = snapshotValue["goal_type"] as! String
    }
    
    func toAnyObject() -> Any {
        return [
            "current_val" : current_val,
            "start_val" : start_val,
            "target_val" : target_val,
            "tracker_id" : tracker_id,
            "exercise" : exercise_name,
            "goal_type" : goal_type
        ]
    }

}
