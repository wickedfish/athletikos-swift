//
//  PastResultsTableVC.swift
//  Athletikos
//
//  Created by Joshua Blasdell on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Firebase

class PastResultsTableVC: UITableViewController {
    
    let statusImages = [UIImage(named: "X.png"), UIImage(named: "check.png")]
    let fire = Fireton.sharedInstance
    var teamID = ""
    var workoutID = ""
    var row = 0
    var relevantWorkouts = [Workout]()
    var allTheUsersForThisWorkout = [String]()
    var didTheUserCompleteTheWorkout = [Bool]()
    var friendlyNames = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Results"
        
        for (userID, workout) in (fire.findTeam(withID: teamID).allCompletedWorkouts[workoutID]?.workoutPerUser)! {
            allTheUsersForThisWorkout.append(userID)
            didTheUserCompleteTheWorkout.append(workout.workout_completed)
            friendlyNames.append(fire.allUsers[userID]!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allTheUsersForThisWorkout.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PRCell", for: indexPath) as! PastResultsCell

        cell.Name.text = friendlyNames[(indexPath as NSIndexPath).row]
        if(didTheUserCompleteTheWorkout[(indexPath as NSIndexPath).row]){
            cell.ResultImage.image = UIImage(named: "check.png")
        }else{
            cell.ResultImage.image = UIImage(named: "X.png")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row
        
        return indexPath
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = (segue.destination as! ResultsTable)
        
        for(_,workout) in (fire.findTeam(withID: teamID).allCompletedWorkouts[workoutID]?.workoutPerUser)!{
            relevantWorkouts.append(workout)
        }
        
        //print("relevant workouts row time elapsed: \(relevantWorkouts[row].time_elapsed)")
        dest.completedWorkout = relevantWorkouts[row]
        //dest.completedWorkout.time_elapsed =
    }
}
