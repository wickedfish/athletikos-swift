//
//  SignUpVC.swift
//  Athletikos
//
//  Created by Daniel Porter on 3/26/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import EZLoadingActivity

class SignUpVC: UIViewController {
    @IBOutlet weak var emailTextField: JVFloatLabeledTextField!
    @IBOutlet weak var passwordTextField: JVFloatLabeledTextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    let defaults = UserDefaults.standard
    let fire = Fireton.sharedInstance
    var alreadyLaidout = false
    
    let style = Styleton.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //Styling
        view.backgroundColor = style.blue
        
        signUpButton.setTitleColor(style.white, for: .normal)
        cancelButton.setTitleColor(style.white, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if(!alreadyLaidout) {
            let bottomLineEMAIL = CALayer()
            bottomLineEMAIL.frame = CGRect(x: 0.0, y: emailTextField.frame.height + 1, width: emailTextField.frame.width, height: 2.0)
            bottomLineEMAIL.backgroundColor = style.white.cgColor
            emailTextField.borderStyle = .none
            emailTextField.layer.addSublayer(bottomLineEMAIL)
            emailTextField.textColor = style.white
            emailTextField.floatingLabelTextColor = style.white
            emailTextField.floatingLabelActiveTextColor = style.white
            emailTextField.floatingLabelFont = emailTextField.floatingLabelFont.withSize(13)
            
            let bottomLinePWD = CALayer()
            bottomLinePWD.frame = CGRect(x: 0.0, y: passwordTextField.frame.height + 1, width: passwordTextField.frame.width, height: 2.0)
            bottomLinePWD.backgroundColor = style.white.cgColor
            passwordTextField.borderStyle = .none
            passwordTextField.layer.addSublayer(bottomLinePWD)
            passwordTextField.floatingLabelTextColor = style.white
            passwordTextField.floatingLabelActiveTextColor = style.white
            passwordTextField.floatingLabelFont = passwordTextField.floatingLabelFont.withSize(13)
            passwordTextField.textColor = style.white

            
            alreadyLaidout = true
        }
    }

    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func createAccountAction(_ sender: AnyObject) {
        if (emailTextField.text == "" || passwordTextField.text == "") {
            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            EZLoadingActivity.show("Signing Up...", disableUI: true)
            FIRAuth.auth()?.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                
                if error == nil {
                    EZLoadingActivity.Settings.SuccessText = "Verification Sent"
                    EZLoadingActivity.hide(true, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        print("User successfully created")
                        user?.sendEmailVerification(completion: nil)
                        
                        self.performSegue(withIdentifier: "signUpCompleted", sender: self)
                    })
                } else {
                    EZLoadingActivity.hide(false, animated: true)
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
