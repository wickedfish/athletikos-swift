//
//  TrackedExercise.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 4/1/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import Firebase

struct TrackedExercise {
    var exercise_name : String
    var total_reps : Int
    var total_sets : Int
    var total_weight : Int
    
    init() {
        exercise_name = "null"
        total_reps = 0
        total_sets = 0
        total_weight = 0
    }
    
    init(snapshot: FIRDataSnapshot, exercise_name: String) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        self.exercise_name = exercise_name
        
        if ((snapshotValue["total_reps"]) != nil) {
            total_reps = snapshotValue["total_reps"] as! Int
        }
        else {
            total_reps = 0
        }
        
        if ((snapshotValue["total_sets"]) != nil) {
            total_sets = snapshotValue["total_sets"] as! Int
        }
        else {
            total_sets = 0
        }
        
        if ((snapshotValue["total_weight"]) != nil) {
            total_weight = snapshotValue["total_weight"] as! Int
        }
        else {
            total_weight = 0
        }
    }
    
    func toAnyObject() -> Any {
    return ["total_reps" : total_reps,
            "total_sets" : total_sets,
            "total_weight" : total_weight
        ]
    }
}
