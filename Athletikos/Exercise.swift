//
//  Exercise.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/6/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import Firebase

struct Exercise {
    var exercise_name : String
    var reps : Int
    var weight : Int
    var image_url : String
    var comment : String
    var sets : Int
    var exercise_completed  : Bool
    var exercise_description : String
    let ref: FIRDatabaseReference?
    var exercise_placement : Int //This is the order in which exercises should appear
    
    init(exercise_name: String, reps: Int, weight: Int, image_url: String, comment: String, sets: Int) {
        
        self.exercise_name = exercise_name
        self.reps = reps
        self.weight = weight
        self.image_url = image_url
        self.comment = comment
        self.sets = sets
        self.ref = nil
        self.exercise_completed = false
        self.exercise_description = "null"
        self.exercise_placement = 0
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        //Exercise Name
        if ((snapshotValue["exercise_name"]) != nil) {
            exercise_name = snapshotValue["exercise_name"] as! String
        }
        else {
            exercise_name = "null"
        }
        
        //Reps
        if ((snapshotValue["reps"]) != nil) {
            reps = snapshotValue["reps"] as! Int
        }
        else {
            reps = 0
        }
        
        //Weight
        if (((snapshotValue["weight"]) != nil) && !(snapshotValue["weight"] is NSString)) {
            weight = snapshotValue["weight"] as! Int
        }
        else {
            weight = 0
        }
        
        //Image
        if ((snapshotValue["image"]) != nil) {
            image_url = snapshotValue["image"] as! String
        }
        else {
            image_url = "null"
        }
        
        //Comment
        if ((snapshotValue["comment"]) != nil) {
            comment = snapshotValue["comment"] as! String
        }
        else {
            comment = "null"
        }
        
        //Sets
        if ((snapshotValue["sets"]) != nil) {
            sets = snapshotValue["sets"] as! Int
        }
        else {
            sets = 0
        }
        
        //Exercise Completed boolean
        if ((snapshotValue["exercise_completed"]) != nil) {
            exercise_completed = snapshotValue["exercise_completed"] as! Bool
        }
        else {
            exercise_completed = false
        }
        
        //Exercise description string
        if ((snapshotValue["exercise_description"]) != nil) {
            exercise_description = snapshotValue["exercise_description"] as! String
        }
        else {
            exercise_description = "null"
        }
        
        ref = snapshot.ref
        
        //Exercise_Placement
        if ((snapshotValue["exercise_placement"]) != nil) {
            exercise_placement = snapshotValue["exercise_placement"] as! Int
        }
        else {
            exercise_placement = 0
        }
        


    }
    
    func toAnyObject() -> Any {
        return [
            "exercise_name": exercise_name,
            "reps": reps,
            "weight": weight,
            "sets": sets,
            "exercise_completed": exercise_completed
        ]
    }


}
