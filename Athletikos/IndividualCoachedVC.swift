//
//  IndividualCoachedVC.swift
//  Athletikos
//
//  Created by Joshua Blasdell on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

private let reuseIdentifier = "CoachedGoalCell"

class IndividualCoachedVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let fire = Fireton.sharedInstance
    var teamID = ""
    var teamName = String()
    var row = 0
    //workout object for past workouts
    var workouts = [Workout]()
    let style = Styleton.sharedInstance
    
    var upcomingRoutines = [(String , Date, DateComponents)]()
    var pastRoutines = [(String , Date, DateComponents)]()
    
    @IBOutlet weak var nextWorkoutTable: UITableView!
    @IBOutlet weak var completedWorkoutTable: UITableView!
    @IBOutlet weak var goalCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Styling
        view.backgroundColor = style.blue
        
        nextWorkoutTable.layer.borderWidth = 3
        nextWorkoutTable.layer.borderColor = style.blue.cgColor
        nextWorkoutTable.layer.cornerRadius = 10
        completedWorkoutTable.layer.borderWidth = 3
        completedWorkoutTable.layer.borderColor = style.blue.cgColor
        completedWorkoutTable.layer.cornerRadius = 10
        goalCollection.layer.borderWidth = 3
        goalCollection.layer.borderColor = style.blue.cgColor
        goalCollection.layer.cornerRadius = 10
        
        self.title = teamName
                
        let theTeam = fire.findTeam(withID: teamID)
        var workoutIDs = [String]()
        for (ID, completedWorkoutObject) in theTeam.allCompletedWorkouts {
            workoutIDs.append(ID)
            var tempWorkout = fire.findWorkout(withID: ID)
            tempWorkout.day = completedWorkoutObject.dayCompleted
            tempWorkout.month = completedWorkoutObject.monthCompleted
            workouts.append(tempWorkout)
        }

        nextWorkoutTable.reloadData()
        completedWorkoutTable.reloadData()
        
        //orders workouts by date
        loadUpcomingRoutines()
        loadPastRoutines()
    }
    
    func loadUpcomingRoutines(){
        let team = fire.findTeam(withID: teamID)
        
        for workout in team.workouts{
            for routine in fire.workouts{
                if workout.workout_id == workout.workout_id{
                    var workoutDate = DateComponents()
                    workoutDate.day = workout.day
                    workoutDate.month = workout.month
                    workoutDate.year = workout.year
                            
                    let cal = Calendar.current
                            
                    upcomingRoutines.append((routine.workout_name, cal.date(from: workoutDate)!,workoutDate))
                }
            }
        }
        upcomingRoutines.sort{$0.1 < $1.1}
    }
    
    func loadPastRoutines(){
        for workout in workouts{
            var workoutDate = DateComponents()
            workoutDate.day = workout.day
            workoutDate.month = workout.month
            workoutDate.year = workout.year
            
            let cal = Calendar.current
            
            pastRoutines.append((workout.workout_name, cal.date(from: workoutDate)!,workoutDate))
        }
        pastRoutines.sort{$0.1 < $1.1}
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for team in fire.teams{
            if team.team_id == teamID{
                if(workouts.count > 4){
                    return 4
                }else{
                    return workouts.count
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == nextWorkoutTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nextCoachedCell", for: indexPath) as! PersonalWorkoutCell
            cell.workoutLabel.text = upcomingRoutines[(indexPath as NSIndexPath).row].0
            var workoutDateComponents = upcomingRoutines[(indexPath as NSIndexPath).row].2
            
            let dateFormatter: DateFormatter = DateFormatter()
            let months = dateFormatter.shortMonthSymbols
            let monthSymbol = months?[workoutDateComponents.month!-1]
            let cal = Calendar.current
            
            if(cal.isDateInToday(upcomingRoutines[(indexPath as NSIndexPath).row].1) ){
                cell.dateLabel.text = "Today"
            }else {
                cell.dateLabel.text = "\(monthSymbol!) \(workoutDateComponents.day!)"
                
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pastCoachedCell", for: indexPath) as! PersonalWorkoutCell
            let stuff = (indexPath as NSIndexPath).row
            cell.workoutLabel.text = pastRoutines[stuff].0
            var workoutDateComponents = pastRoutines[(indexPath as NSIndexPath).row].2
            
            let dateFormatter: DateFormatter = DateFormatter()
            let months = dateFormatter.shortMonthSymbols
            let monthSymbol = months?[workoutDateComponents.month!-1]
            let cal = Calendar.current
            
            if(cal.isDateInToday(pastRoutines[(indexPath as NSIndexPath).row].1) ){
                cell.dateLabel.text = "Today"
            }else {
                cell.dateLabel.text = "\(monthSymbol!) \(workoutDateComponents.day!)"
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row
        
        return indexPath
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        for team in fire.teams{
            if team.team_id == teamID{
                if(team.goals.count == 1){
                    let totalCellWidth =  169 * team.goals.count
                    let totalSpacingWidth = 10 * (team.goals.count - 1)
            
                    let leftInset = ((self.goalCollection.frame.width/2) - CGFloat(totalCellWidth + totalSpacingWidth) / 2)
                    let rightInset = leftInset
            
                    return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
                } else if(team.goals.count == 2){
                    let totalCellWidth =  169 * team.goals.count
                    let totalSpacingWidth = 10 * (team.goals.count - 1)
            
                    let leftInset = ((self.goalCollection.frame.width/2) - CGFloat(Int(totalCellWidth) + totalSpacingWidth)/2)
                    let rightInset = leftInset
            
                    return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
                }
            }
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        for team in fire.teams{
            if team.team_id == teamID{
                if(team.goals.count > 4){
                    return 4
                }else{
                    return team.goals.count
                }
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GoalCollectionCell
        for team in fire.teams{
            if team.team_id == teamID{
                for tgoal in team.goals{
                    let currentValue = tgoal.current_val
                    let target = tgoal.target_val
                    
                    
                    cell.goalLabel.text = tgoal.tracker_id
                    
                    if(target > currentValue){
                        cell.showProgress(goal: Double(target), progress: Double(currentValue))
                    } else {
                        cell.showProgress(goal: Double(target), progress: Double(1.0))
                    }
                }
            }
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInProgressFromCoachedTeams" {
            
            let dest = segue.destination as! WorkoutVC
            dest.seguedFromTeamPage = true
            dest.seguedFromCoachedPage = true
            dest.isCoachedWorkout = true
            let team = fire.findTeam(withID: teamID)
            dest.workout_id = team.workouts[row].workout_id //*** SET this to the relevant workoutID
        }else if segue.identifier == "toResultsfromCoached" {
                
            let dest = segue.destination as! PastResultsTableVC
            dest.teamID = fire.coachedTeams[row]
            dest.workoutID = fire.findTeam(withID: teamID).workouts[row].workout_id
        }
    }
}


