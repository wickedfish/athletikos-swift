//
//  Team.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 2/28/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation

struct Team {
    var team_id : String
    var coach_id : String
    var dataTracking = [String:Int]()
    var goals = [Goal]()
    var team_members = [String]()
    var team_name : String
    var workouts = [Workout]() //An array of all the team's workouts. The only properties are date (i.e., day, month, year) and workoutID
    var allCompletedWorkouts = [String : TeamCompletedWorkout]() //key is a workout_id, value is the completed workout data.
    var exercisesTracked = [String : TrackedExercise]() //All exercises that the team is tracking. the exercise_name is the key.
    var workoutCompletionData : WorkoutCompletionObject
    
    
    init() {
        team_id = "null"
        coach_id = "null"
        team_name = "null"
        workoutCompletionData = WorkoutCompletionObject()
    }
}
