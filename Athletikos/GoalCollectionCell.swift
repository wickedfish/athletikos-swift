//
//  GoalCollectionCell.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/11/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class GoalCollectionCell: UICollectionViewCell {
    @IBOutlet weak var goalCircleProgression: CircleProgressView!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var goalProgressLabel: UILabel!
    
    func showProgress(goal: Double, progress: Double){
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumSignificantDigits = 2
        formatter.maximumSignificantDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.usesSignificantDigits = true
        
        let progress = (goal - progress) / goal
        goalCircleProgression.setProgress(progress, animated: true)
        
        var trueProgress: Double!
        trueProgress = progress * 100
        goalProgressLabel.text = formatter.string(from: trueProgress as NSNumber)! + "%"
    }
}
