//
//  WorkoutVC.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 3/22/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Firebase

class WorkoutVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerPreviewingDelegate {
    
    var workout_id : String = "1138" //This needs to be set by any view before it segues to this view.
    var exercises = [Exercise]() //This array holds the workout
    let fire = Fireton.sharedInstance //The singleton Firebase class
    var workout = Workout() //Holds the workout object. This object is edited as the workout is being completed and is pushed to Firebase once they leave.
    var timeElapsed = 0 //Holds time
    var workoutTimer : Timer!
    var isRunning = true
    var completionTracker = [Int:Bool]() //For each exercise index, record whether or not the user has completed/skipped that exercise.
    var colorTracker = [Int:Bool]() //For each exercise index, record if the text color should be red (false) or green (true)
    var row : Int = 0
    var seguedFromTeamPage = false
    var seguedFromCoachedPage = false
    var seguedFromPersonalPage = false
    var isPersonalWorkout = false
    var isCoachedWorkout = false
    var isTeamWorkout = false
    var IDOfTeamThatSentTheWorkout = "null"
    var workoutIsInProgress = false
    var stringSource = Stringleton.sharedInstance
    var dateComponentsOfWorkout = DateComponents()
    
    
    var debugTimer : Timer!
    
    var restBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var timeLabel : UILabel!
    
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var startButton : UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        } else {
            print("3D Touch Not Available")
        }
        
        //MARK: Fireton
        workout = fire.findWorkout(withID: workout_id)
        self.navigationItem.title = "\(workout.workout_name)"
        if dateComponentsOfWorkout.day != nil {
            workout.day = dateComponentsOfWorkout.day!
        }
        if dateComponentsOfWorkout.month != nil {
            workout.month = dateComponentsOfWorkout.month!
        }
        if dateComponentsOfWorkout.year != nil {
            workout.year = dateComponentsOfWorkout.year!
        }
        
        //startButton.isHidden = true
        
        //let athletikosBlue = UIColor(colorLiteralRed: 64, green: 153, blue: 255, alpha: 0.5)
        
        //timeLabel.alpha = 0.80
        if !seguedFromTeamPage && !isFutureWorkout() {
            
            startButton.backgroundColor = startButton.tintColor
            startButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            restBarButtonItem = UIBarButtonItem(title: "Rest",
                                                style: .plain,
                                                target: self,
                                                action: #selector(restButtonDidTouch))
            
        }
        else {
            startButton.backgroundColor = UIColor.gray
            startButton.setTitle("Workout Preview", for: UIControlState.normal)
            startButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            startButton.isEnabled = false
        }
        
        
        //workoutTimer = Timer.scheduledTimer(timeInterval: 1.0,			target: self, 			selector: #selector(eachSecond),			userInfo: nil,			repeats: true)
        
        
        //restBarButtonItem.tintColor = UIColor.blue //Doesn't have to be blue
        navigationItem.rightBarButtonItem = restBarButtonItem
        
        //An observer that sets up the tableView when all Firebase data has finished downloading
        //fire.addObserver(self, forKeyPath: "hasFinishedDownloading", options: .new, context: nil)
        
        
        
        //Populate completion tracker and color tracker
        var i : Int = 0
        for _ in workout.exercises {
            completionTracker.updateValue(false, forKey: i)
            colorTracker.updateValue(false, forKey: i)
            i = i + 1
        }
        
        //if seguedFromTeamPage && isFutureWorkout() {
            //self.navigationItem.title = "Preview: \(workout.workout_name)"
            //self.tableView.allowsSelection = false
        //}
        
        //self.tableView.reloadData()
        tableView.reloadData()


        // Do any additional setup after loading the view.
    }
    
    //This function will run when the user taps "back" from the ExerciseDetails screen.
    @IBAction func backFromExerciseDetails(_ segue: UIStoryboardSegue) {
        let exerciseDetailsSource = segue.source
        let ed = exerciseDetailsSource as! TapExDetailsVC
        
        //This row number tells us which exercise has been modified.
        let rowOfTheExercise = ed.exercise_row
        
        //Update reps if the user changed the value
        if (ed.dialOne?.userDidChangeValue)! {
            let newReps = ed.dialOne?.currentValue
            workout.exercises[rowOfTheExercise].reps = newReps!
        }
        
        //Update weight if the user changed the value
        if (ed.dialTwo?.userDidChangeValue)! {
            let newWeight = ed.dialTwo?.currentValue
            workout.exercises[rowOfTheExercise].weight = newWeight!
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func startButtondidTap() {
        if !seguedFromTeamPage && !isFutureWorkout() {
            startButton.isHidden = true
            workoutIsInProgress = true
            
            let runLoop = RunLoop.current
            
            workoutTimer = Timer.scheduledTimer(timeInterval: 1.0,			target: self, 			selector: #selector(eachSecond),			userInfo: nil,			repeats: true)
            
            runLoop.add(workoutTimer, forMode: .commonModes)
            runLoop.add(workoutTimer, forMode: .UITrackingRunLoopMode)
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        //Get the true location of the tableView (Doesn't take the navbar into account)
        let realLoc = CGPoint(x: location.x, y: location.y - tableView.frame.origin.y)
        
        if let indexPath = tableView.indexPathForRow(at: realLoc) {
            if let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)) {
                
                stringSource.reps = self.workout.exercises[row].reps
                stringSource.weight = self.workout.exercises[row].weight
                stringSource.isDialOne = true
                
                guard let exerciseDetailViewController = storyboard?.instantiateViewController(withIdentifier: "TapExDetailsVC") as? TapExDetailsVC else { return nil }
                
                
                exerciseDetailViewController.exercise_name = self.workout.exercises[indexPath.row].exercise_name
                exerciseDetailViewController.exercise_row = indexPath.row
                exerciseDetailViewController.startingReps = self.workout.exercises[indexPath.row].reps
                exerciseDetailViewController.startingWeight = self.workout.exercises[indexPath.row].weight
                exerciseDetailViewController.preferredContentSize = CGSize(width: 0.0, height: 600)
                
                //Get the true location of the cell (Doesn't take the navbar into account)
                let trueRect = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y + tableView.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
                
                previewingContext.sourceRect = trueRect
                
                return exerciseDetailViewController
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
    func eachSecond() {
        timeElapsed = timeElapsed + 1
        formatTime()
        //tableView.reloadData()
    }
    
    //This function formats the text of the time label.
    func formatTime() {
        if timeElapsed >= 60 {
            let minutes = timeElapsed / 60
            let seconds = timeElapsed % 60
            
            if seconds < 10 {
                if minutes < 10 {
                    timeLabel.text = "0\(minutes):0\(seconds)"
                }
                else {
                    timeLabel.text = "\(minutes):0\(seconds)"
                }
            } else {
                if minutes < 10 {
                    timeLabel.text = "0\(minutes):\(seconds)"
                }
                else {
                    timeLabel.text = "\(minutes):\(seconds)"
                }
            }
        }
        else  {//If it's less than 60 seconds
            if timeElapsed < 10 {
                timeLabel.text = "00:0\(timeElapsed)"
            } else {
                timeLabel.text = "00:\(timeElapsed)"
            }
        }
        
    }
    
    func restButtonDidTouch() {
        
        //Only execute if this is a personal workout or a team workout for the current day.
        if !seguedFromTeamPage && !isFutureWorkout() && workoutIsInProgress {
        
            //Stop the timer and stuff
            if isRunning {
                
                
                
                workoutTimer.invalidate()
                isRunning = false
                timeLabel.isHighlighted = true
                timeLabel.highlightedTextColor = self.view.tintColor
            
            }
            else {
                //workoutTimer.fire() //Supposed to make it go.
                let runLoop = RunLoop.current
                
                workoutTimer = Timer.scheduledTimer(timeInterval: 1.0,			target: self, 			selector: #selector(eachSecond),			userInfo: nil,			repeats: true)
                
                runLoop.add(workoutTimer, forMode: .commonModes)
                runLoop.add(workoutTimer, forMode: .UITrackingRunLoopMode)
                
                isRunning = true
                timeLabel.isHighlighted = false
            }
        
            //Flip the title back-and-forth when tapped.
            if (restBarButtonItem.title == "Rest") {
                restBarButtonItem.title = "Resume"
            }
            else {
                restBarButtonItem.title = "Rest"
            }
            //self.tableView.reloadData()
            //tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workout.exercises.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WorkoutCell
        cell.exercise_name.text = workout.exercises[indexPath.row].exercise_name
        
        if completionTracker[indexPath.row]! { //If the cell has been marked as completed/skipped
            if colorTracker[indexPath.row]! { //If the cell is completed
                cell.exercise_name.textColor = UIColor.green
            }
            else { //If the cell is skipped
                cell.exercise_name.textColor = UIColor.red
            }
        }
        else {
            cell.exercise_name.textColor = UIColor.black
        }
        
        if workout.exercises[indexPath.row].reps != 0 {
            cell.reps.text = "\(workout.exercises[indexPath.row].reps)"
        }
        else {
            cell.reps.text = "N/A"
        }
        
        if workout.exercises[indexPath.row].weight != 0 {
            cell.weight.text = "\(workout.exercises[indexPath.row].weight)"
        }
        else {
            cell.weight.text = "0"
        }
        
        if workout.exercises[indexPath.row].sets != 0 {
            cell.sets.text = "\(workout.exercises[indexPath.row].sets)"
        }
        else {
            cell.sets.text = "1"
        }

        
        return cell
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?  {
        
        //if !seguedFromTeamPage && !isFutureWorkout() {
    
            let skip = UITableViewRowAction(style: .default, title: "Skip", handler: {action, indexPath in
                if !self.seguedFromTeamPage && !self.isFutureWorkout() && self.workoutIsInProgress {
                    self.workout.exercises[indexPath.row].exercise_completed = false
            
                    //Note: A true below means that the completion value has been set in this view. A change from false to false still counts. We're just making sure the user has completed/skipped each workout at least once
                    self.completionTracker.updateValue(true, forKey: indexPath.row)
                    self.colorTracker.updateValue(false, forKey: indexPath.row)
                    if self.isFinishedWithWorkout() {
                        self.performSegue(withIdentifier: "toWorkoutResultsFromWorkoutVC", sender: nil)
                    }
                }
                //self.tableView.reloadData()
                tableView.reloadData()
                
            })
        
            let complete = UITableViewRowAction(style: .normal, title: "Complete", handler: {action2, indexPath in
                if !self.seguedFromTeamPage && !self.isFutureWorkout() && self.workoutIsInProgress {
                    self.workout.exercises[indexPath.row].exercise_completed = true
                    self.completionTracker.updateValue(true, forKey: indexPath.row)
                    self.colorTracker.updateValue(true, forKey: indexPath.row)
                    if self.isFinishedWithWorkout() {
                        self.performSegue(withIdentifier: "toWorkoutResultsFromWorkoutVC", sender: nil)
                    }
                }
                //self.tableView.reloadData()
                tableView.reloadData()
                
            })
        
            complete.backgroundColor = UIColor.green
        
        //tableView.reloadData()
        //tableView.reloadData()
        
            return [skip, complete]
        //}
        //return [nil, nil]
    }
    
    func isFinishedWithWorkout() -> Bool {
        for (_ , isComplete) in completionTracker {
            if !isComplete {
                return false
            }
        }
        return true //Because no falses were found within the dictionary.
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true) //Makes things pretty
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWorkoutResultsFromWorkoutVC" {
            workout.workout_completed = true //Mark the workout as completed
            
            workout.time_elapsed = timeElapsed
            
            let dest = segue.destination as! ResultsTable
            dest.completedWorkout = self.workout
            dest.userJustCompletedWorkout = true
            
            //MARK: Add results to Firebase
            
            let date = Date()
            let calendar = Calendar.current
            let day = calendar.component(.day, from: date)
            let month = calendar.component(.month, from: date)
            let year = calendar.component(.year, from: date)
            let dayOfWeek = calendar.component(.weekday, from: date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let monthFormatter = DateFormatter()
            monthFormatter.dateFormat = "MMMM"
            let currentMonthString = monthFormatter.string(from: date)
            var i = 0
            
            let userCompletedRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/completed_workouts/\(workout.workout_id)")
            let userCompletedDateRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/completed_workouts/\(workout.workout_id)/date_completed")
            
            
            userCompletedRef.setValue(["day_incrementor":1,"workout_completed" : workout.workout_completed, "time_elapsed" : workout.time_elapsed])
            userCompletedDateRef.setValue(["day" : day , "month" : month , "year" : year])
            for exercise in workout.exercises {
                let userCompletedExerciseRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/completed_workouts/\(workout.workout_id)/exercises/\(i)")
                userCompletedExerciseRef.setValue(exercise.toAnyObject())
                i = i + 1
            }
            
            
            //MARK - Update User Data Tracking
            //MARK - User exercises_tracked
            let userExerciseTrackingRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/data_tracking/exercises_tracked")
            let userWorkoutCompletionTrackingRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/data_tracking/workout_completion")
            for (exercise_name, exerciseObject) in fire.userExercisesTracked {
                for exercise in workout.exercises {
                    if exercise.exercise_name == exercise_name {
                        let exerciseTrackingRef = userExerciseTrackingRef.child("\(exercise_name)")
                        var tempTrackedExercise = exerciseObject
                        tempTrackedExercise.total_reps = tempTrackedExercise.total_reps + exercise.reps
                        tempTrackedExercise.total_sets = tempTrackedExercise.total_sets + exercise.sets
                        tempTrackedExercise.total_weight = tempTrackedExercise.total_weight + exercise.weight
                        
                        exerciseTrackingRef.setValue(tempTrackedExercise.toAnyObject())
                    }
                }
            }
            //MARK - User workout_completion
            //Increment the total number of workouts completed.
            let userTotalWorkoutRef = userWorkoutCompletionTrackingRef.child("total_workouts")
            let newTotalWorkoutNumber = fire.userWorkoutCompletionData.total_workouts + 1
            userTotalWorkoutRef.setValue(newTotalWorkoutNumber)
            
            //Increment the number of workouts for that day.
            var theNewNumberOfWorkouts = 0
            //But we have to set it for the appropriate month
            
            let monthDayBranchRef = userWorkoutCompletionTrackingRef.child("\(currentMonthString)/\(day)")
            //If this date already has a value, add to it
            if fire.userWorkoutCompletionData.monthData["\(day)"] != nil && fire.userWorkoutCompletionData.monthThatDataIsFor == currentMonthString {
                theNewNumberOfWorkouts = fire.userWorkoutCompletionData.monthData["\(day)"]! + 1
            }
            else { //else, the value is now 1
                theNewNumberOfWorkouts = 1
            }
            monthDayBranchRef.setValue(theNewNumberOfWorkouts)

            
            //Now, we'll increment the value in the week branch
            
            let userWorkoutCompletionWeekRef = userWorkoutCompletionTrackingRef.child("week")
            let startDate = dateFormatter.date(from: "\(fire.userWorkoutCompletionData.weekStartMonth) \(fire.userWorkoutCompletionData.weekStartDay) \(fire.userWorkoutCompletionData.weekStartYear)")
            var timeSinceStartDate = 0.0
            if startDate != nil {
                timeSinceStartDate = date.timeIntervalSince(startDate!)
            }
            else {
                print("Error: there was no start date for user week data_tracking in Firebase")
            }
            let sevenDaysTime = 604800
            let aSingleWorkout = 1
            //print("timeSinceStartInt: \(Int(timeSinceStartDate)) timeSinceStart: \(timeSinceStartDate) sevenDaysTime: \(sevenDaysTime)")
            
            //If it's been a week or more since the current tracking week started, start it over. (Set start date to today and add the day of week of value
            if Int(timeSinceStartDate) >= sevenDaysTime {
                //print("The result is: \(Int(timeSinceStartDate) >= sevenDaysTime)")
                //print("The if-block fired.")
                userWorkoutCompletionWeekRef.removeValue() //clear the old stuff
                /*
                let userWorkoutCompletionWeekStartDateRef = userWorkoutCompletionWeekRef.child("start_date")
                userWorkoutCompletionWeekStartDateRef.setValue(["day" : day,
                                                                "month" : month,
                                                                "year" : year])
                */
                userWorkoutCompletionWeekRef.setValue(["\(dayOfWeek)" : aSingleWorkout,
                                                       "start_date" : ["day" : day,
                                                                       "month" : month,
                                                                       "year" : year]])
            }
            else { //Else, just increment the day of week value
                var currentWorkoutNumberForDay = 0
                if fire.userWorkoutCompletionData.week["\(dayOfWeek)"] != nil {
                    currentWorkoutNumberForDay = fire.userWorkoutCompletionData.week["\(dayOfWeek)"]!
                }
                userWorkoutCompletionWeekRef.updateChildValues(["\(dayOfWeek)" : (currentWorkoutNumberForDay + 1)])
                //userWorkoutCompletionWeekRef.setValue(["\(dayOfWeek)" : (currentWorkoutNumberForDay + 1)])
            }
            
            //MARK - Data Tracking stuff specific to Personal or Team workouts.
            
            let userPersonalWorkoutsRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/personal_workouts/\(workout.workout_id)")

            if isPersonalWorkout {
                userPersonalWorkoutsRef.removeValue() //The workout is completed. Remove it from the branch of current workouts
                
                for goal in fire.goals {
                    for exercise in workout.exercises {
                        
                        //check if any of the exercises correspond to goals the user has
                        if goal.exercise_name == exercise.exercise_name && exercise.exercise_completed {
                            let userGoalRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(fire.userID!)/goals/\(goal.goal_key)")
                            var tempGoal = goal
                            
                            //Figure out which data to add to the goal
                            if tempGoal.goal_type == "weight" {
                                print("Weight: \(exercise.weight) Sets: \(exercise.sets) Reps: \(exercise.sets)")
                                tempGoal.current_val = tempGoal.current_val + (exercise.weight * exercise.reps * exercise.sets)
                            }
                            else if tempGoal.goal_type == "reps" {
                                tempGoal.current_val = tempGoal.current_val + (exercise.reps * exercise.sets)
                            }
                            else {
                                print("WorkoutVC - Upload to personal branch - Unrecognized goal_type")
                            }
                            
                            userGoalRef.setValue(tempGoal.toAnyObject())
                        }
                    }
                }
            }
            else if isTeamWorkout {
                var theTeamThatSentTheWorkout = fire.findTeam(withID: IDOfTeamThatSentTheWorkout)
                
                //MARK - Upload Team Goals, if applicable
                for goal in theTeamThatSentTheWorkout.goals {
                    for exercise in workout.exercises {
                        if goal.exercise_name == exercise.exercise_name {
                            let teamGoalRef = FIRDatabase.database().reference(withPath: "athletikos_data/teams/\(IDOfTeamThatSentTheWorkout)/goals/\(goal.goal_key)")
                            var tempGoal = goal
                            
                            if tempGoal.goal_type == "weight" {
                                tempGoal.current_val = tempGoal.current_val + (exercise.weight * exercise.sets * exercise.reps)
                            }
                            else if tempGoal.goal_type == "reps" {
                                tempGoal.current_val = tempGoal.current_val + (exercise.sets * exercise.reps)
                            }
                            else {
                                print("WorkoutVC - Upload to team branch - Unrecognized goal_type")
                            }
                            
                            teamGoalRef.setValue(tempGoal.toAnyObject())

                        }
                    }
                }
                
                //MARK - Team exercises_tracked
                let teamExerciseTrackingRef = FIRDatabase.database().reference(withPath: "athletikos_data/teams/\(IDOfTeamThatSentTheWorkout)/data_tracking/exercises_tracked")
                let teamWorkoutCompletionTrackingRef = FIRDatabase.database().reference(withPath: "athletikos_data/teams/\(IDOfTeamThatSentTheWorkout)/data_tracking/workout_completion")
                for (exercise_name, exerciseObject) in theTeamThatSentTheWorkout.exercisesTracked {
                    for exercise in workout.exercises {
                        if exercise.exercise_name == exercise_name {
                            let exerciseTrackingRef = teamExerciseTrackingRef.child("\(exercise_name)")
                            var tempTrackedExercise = exerciseObject
                            tempTrackedExercise.total_reps = tempTrackedExercise.total_reps + exercise.reps
                            tempTrackedExercise.total_sets = tempTrackedExercise.total_sets + exercise.sets
                            tempTrackedExercise.total_weight = tempTrackedExercise.total_weight + exercise.weight
                            
                            exerciseTrackingRef.setValue(tempTrackedExercise.toAnyObject())
                        }
                    }
                }
                //MARK - Team workout_completion
                //Increment the total number of workouts completed.
                let teamTotalWorkoutRef = teamWorkoutCompletionTrackingRef.child("total_workouts")
                let newTotalTeamWorkoutNumber = theTeamThatSentTheWorkout.workoutCompletionData.total_workouts + 1
                teamTotalWorkoutRef.setValue(newTotalTeamWorkoutNumber)
                
                //Increment the number of workouts for that day.
                var theNewNumberOfWorkouts = 0
                
                //But we have to set it for the appropriate month
                let monthDayBranchRef = teamWorkoutCompletionTrackingRef.child("\(currentMonthString)/\(day)")
                //If this date already has a value, add to it
                if theTeamThatSentTheWorkout.workoutCompletionData.monthData["\(day)"] != nil && fire.userWorkoutCompletionData.monthThatDataIsFor == currentMonthString {
                    theNewNumberOfWorkouts = theTeamThatSentTheWorkout.workoutCompletionData.monthData["\(day)"]! + 1
                }
                else { //else, the value is now 1
                    theNewNumberOfWorkouts = 1
                }
                monthDayBranchRef.setValue(theNewNumberOfWorkouts)
                
                //Now, we'll increment the value in the week branch
                
                let teamWorkoutCompletionWeekRef = teamWorkoutCompletionTrackingRef.child("week")
                let startDate = dateFormatter.date(from: "\(theTeamThatSentTheWorkout.workoutCompletionData.weekStartMonth) \(theTeamThatSentTheWorkout.workoutCompletionData.weekStartDay) \(theTeamThatSentTheWorkout.workoutCompletionData.weekStartYear)")
                let timeSinceStartDate = date.timeIntervalSince(startDate!)
                let sevenDaysTime = 604800
                let aSingleWorkout = 1
                
                //If it's been a week or more since the current tracking week started, start it over. (Set start date to today and add the day of week of value
                if Int(timeSinceStartDate) >= sevenDaysTime {
                    teamWorkoutCompletionWeekRef.removeValue() //clear the old stuff
                    let teamWorkoutCompletionWeekStartDateRef = teamWorkoutCompletionWeekRef.child("start_date")
                    teamWorkoutCompletionWeekStartDateRef.setValue(["day" : day,
                                                                    "month" : month,
                                                                    "year" : year])
                    teamWorkoutCompletionWeekRef.setValue(["\(dayOfWeek)" : aSingleWorkout, "start_date" : ["day" : day,
                                                        "month" : month,
                                                        "year" : year]])
                }
                else { //Else, just increment the day of week value
                    var currentWorkoutNumberForDay = 0
                    if theTeamThatSentTheWorkout.workoutCompletionData.week["\(dayOfWeek)"] != nil {
                        currentWorkoutNumberForDay = theTeamThatSentTheWorkout.workoutCompletionData.week["\(dayOfWeek)"]!
                    }
                    teamWorkoutCompletionWeekRef.updateChildValues(["\(dayOfWeek)" : (currentWorkoutNumberForDay + 1)])
                    //teamWorkoutCompletionWeekRef.setValue(["\(dayOfWeek)" : (currentWorkoutNumberForDay + 1)])
                }


                let teamCompletedRef = FIRDatabase.database().reference(withPath: "athletikos_data/teams/\(IDOfTeamThatSentTheWorkout)/completed_workouts/\(workout.workout_id)/\(fire.userID!)")
                let teamCompletedDateRef = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(IDOfTeamThatSentTheWorkout)/completed_workouts/\(workout.workout_id)/date_completed")
                
                
                teamCompletedRef.setValue(["workout_completed" : workout.workout_completed,
                                           "time_elapsed" : workout.time_elapsed])
                teamCompletedDateRef.setValue(["day" : day , "month" : month , "year" : year])
                var j = 0
                for exercise in workout.exercises {
                    let teamCompletedExerciseRef = FIRDatabase.database().reference(withPath: "athletikos_data/teams/\(IDOfTeamThatSentTheWorkout)/completed_workouts/\(workout.workout_id)/\(fire.userID!)/exercises/\(i)")
                    teamCompletedExerciseRef.setValue(exercise.toAnyObject())
                    j = j + 1
                }
            }
            
            
        }
        else if segue.identifier == "toExerciseDetailsFromWorkoutVC" {
            //let dest = segue.destination as! TapExDetailsVC
            let dest2 = segue.destination.childViewControllers[0] as! TapExDetailsVC
            
            dest2.exercise_name = self.workout.exercises[row].exercise_name
            dest2.exercise_row = self.row
            dest2.startingReps = self.workout.exercises[row].reps
            dest2.startingWeight = self.workout.exercises[row].weight
            
            stringSource.reps = self.workout.exercises[row].reps
            stringSource.weight = self.workout.exercises[row].weight
            stringSource.isDialOne = true
            
        }
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isFutureWorkout() -> Bool {
        
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)

        if !seguedFromTeamPage { //Return true if it's from the personal page because personal future workouts can be completed.
            return false
        }
        else if seguedFromCoachedPage { //Return true because coaches can't perform workouts
            return true
        }
        else if workout.year > year {
            return true
        }
        else if workout.month > month {
            return true
        }
        else if workout.day > day {
            return true
        }
        else {
            return false
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
