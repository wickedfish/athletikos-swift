//
//  LoginVC.swift
//  Athletikos
//
//  Created by Daniel Porter on 3/20/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import KeychainSwift
import RevealingSplashView
import EZLoadingActivity

let style = Styleton.sharedInstance

class LoginVC: UIViewController {
    let fire = Fireton.sharedInstance
    let defaults = UserDefaults.standard
    var userID: String!
    let keychain = KeychainSwift()
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: style.logoName)!,iconInitialSize: CGSize(width: 100, height: 100), backgroundColor: style.blue)
    var alreadyLaidout = false

    @IBOutlet weak var sign_inButton: UIButton!
    @IBOutlet weak var sign_upButton: UIButton!
    @IBOutlet weak var passwordTextField: JVFloatLabeledTextField!
    @IBOutlet weak var emailTextField: JVFloatLabeledTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        revealingSplashView.tag = 100
        
        //Page Styling
        view.backgroundColor = style.blue
        
        self.view.addSubview(revealingSplashView)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        sign_inButton.setTitleColor(.white, for: .normal)
        sign_upButton.setTitleColor(.white, for: .normal)
        
        print(emailTextField.frame.width)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if(!alreadyLaidout) {
            let bottomLineEMAIL = CALayer()
            bottomLineEMAIL.frame = CGRect(x: 0.0, y: emailTextField.frame.height + 1, width: emailTextField.frame.width, height: 2.0)
            bottomLineEMAIL.backgroundColor = style.white.cgColor
            emailTextField.borderStyle = .none
            emailTextField.layer.addSublayer(bottomLineEMAIL)
            emailTextField.textColor = style.white
            emailTextField.floatingLabelTextColor = style.white
            emailTextField.floatingLabelActiveTextColor = style.white
            emailTextField.floatingLabelFont = emailTextField.floatingLabelFont.withSize(13)
            
            let bottomLinePWD = CALayer()
            bottomLinePWD.frame = CGRect(x: 0.0, y: passwordTextField.frame.height + 1, width: passwordTextField.frame.width, height: 2.0)
            bottomLinePWD.backgroundColor = style.white.cgColor
            passwordTextField.borderStyle = .none
            passwordTextField.layer.addSublayer(bottomLinePWD)
            passwordTextField.floatingLabelTextColor = style.white
            passwordTextField.floatingLabelActiveTextColor = style.white
            passwordTextField.floatingLabelFont = passwordTextField.floatingLabelFont.withSize(13)
            passwordTextField.textColor = style.white
            
            alreadyLaidout = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(keychain.get("email") == nil || keychain.get("email")! == "" || keychain.get("password") == nil || keychain.get("password")! == "") {
            keychain.set("", forKey: "email")
            keychain.set("", forKey: "password")
            revealingSplashView.startAnimation(){
                print("Completed")
            }
        } else {
            trySavedAccount(email: keychain.get("email")!, password: keychain.get("password")!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Unwinds and clears the UID from memory
    @IBAction func unwindToSignIn(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func unwindSignOut(segue: UIStoryboardSegue) {
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
        
        print("Log out from \((FIRAuth.auth()?.currentUser?.displayName)!)")
        fire.clearData()
        keychain.delete("email")
        keychain.delete("password")
        if FIRAuth.auth()?.currentUser != nil {
            do {
                try FIRAuth.auth()?.signOut()
                print("Logout Succeeded")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func unwindFromSignUp(segue: UIStoryboardSegue) {
        
    }
    
    
    
    func trySavedAccount(email: String, password: String) {
        //Test to see if UID is vaild
        //Print into the console if successfully logged in
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if error == nil {
                print("You have successfully logged in (Automatic)")
                if(user?.isEmailVerified)!{
                    if(user?.displayName != nil) {
                        //Print into the console if successfully logged in
                        //self.fire.configure(ID: (user?.uid)!)
                        print("User already made setup")
                        print((user?.displayName)!)
                        
                        self.fire.configure(ID: (user?.uid)!)
                        
                        self.emailTextField.text = ""
                        self.passwordTextField.text = ""
                        
                        //Go to the HomeViewController if the login is sucessful
                        self.performSegue(withIdentifier: "ToLoadingScreen", sender: nil)
                    } else {
                        self.fire.configure(ID: (user?.uid)!)
                        print("Configure Fired")
                        print((user?.uid)!)
                        
                        self.emailTextField.text = ""
                        self.passwordTextField.text = ""
                        
                        self.performSegue(withIdentifier: "initialProfileSetup", sender: nil)
                    }
                } else {
                    let alertController = UIAlertController(title: "Validation Required", message: "Validation Email Re-Sent to \((user?.email)!)", preferredStyle: .alert)
                    user?.sendEmailVerification(completion: nil)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                print(error?.localizedDescription ?? "Hello")
                self.revealingSplashView.startAnimation(){
                    print("Completed")
                }
            }
        }
    }
    
    
    @IBAction func sign_inAction(_ sender: Any) {
        if(passwordTextField.text == "" || emailTextField.text == "") {
            let alertController = UIAlertController(title: "Error", message: "Please enter username and password", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
        } else {
            EZLoadingActivity.show("Signing-In...", disableUI: true)
            FIRAuth.auth()?.signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                if error == nil {
                    if(user?.isEmailVerified)!{
                        EZLoadingActivity.hide(true, animated: true)
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                            if(user?.displayName != nil) {
                                //Print into the console if successfully logged in
                                print("You have successfully logged in (Button)")
                                self.fire.configure(ID: (user?.uid)!)
                                self.keychain.set(self.emailTextField.text!, forKey: "email")
                                self.keychain.set(self.passwordTextField.text!, forKey: "password")
                                
                                self.emailTextField.text = ""
                                self.passwordTextField.text = ""
                                
                                //Go to the HomeViewController if the login is sucessful
                                self.performSegue(withIdentifier: "ToLoadingScreenWOSplash", sender: nil)
                            } else {
                                self.emailTextField.text = ""
                                self.passwordTextField.text = ""
                                
                                self.fire.configure(ID: (user?.uid)!)
                                self.keychain.set(self.emailTextField.text!, forKey: "email")
                                self.keychain.set(self.passwordTextField.text!, forKey: "password")
                                self.performSegue(withIdentifier: "initialProfileSetupWOSplash", sender: nil)
                            }
                        })
                    } else {
                        EZLoadingActivity.hide()
                        let alertController = UIAlertController(title: "Validation Required", message: "Validation Email Re-Sent to \((user?.email)!)", preferredStyle: .alert)
                        user?.sendEmailVerification(completion: nil)
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    if Reachability.isConnectedToNetwork() {
                        EZLoadingActivity.Settings.FailText = "Invalid Login"
                        EZLoadingActivity.hide(false, animated: true)
                    } else {
                        EZLoadingActivity.Settings.FailText = "Network Unavailable"
                        EZLoadingActivity.hide(false, animated: true)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "initialProfileSetup" {
            let destination = segue.destination as! profileSetupVC
            destination.sentFrom = "login"
        } else if segue.identifier == "ToLoadingScreen" {
            let destination = segue.destination as! LoadingScreenVC
            destination.sentFrom = "login"
        } else if segue.identifier == "initialProfileSetupWOSplash" {
            let destination = segue.destination as! profileSetupVC
            destination.sentFrom = "loginWOSplash"
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
