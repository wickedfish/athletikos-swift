//
//  Fireton.swift
//  
//
//  Created by Speedy Gonzalez on 2/25/17.
//
//

import Foundation
import Firebase
import WatchConnectivity

class Fireton: NSObject, WCSessionDelegate{
    
    var session: WCSession?
    
    var userID : String!
    var coachedTeams = [String]()
    var subscribedTeams = [String]()
    var displayName : String = "null"
    var firstName : String = "null"
    var lastName : String = "null"
    var completedWorkouts = [Workout]()
    var createdExercises = [Exercise]()
    var userExercisesTracked = [String : TrackedExercise]()
    var userWorkoutCompletionData = WorkoutCompletionObject()
    var goals = [Goal]()
    var personalWorkouts = [Workout]()
    var teamWorkouts = [Workout]()
    var userWorkouts = [String]() //An array of workout IDs, from the branch athletikos_data/users/[ID]/workouts
    var workouts = [Workout]()
    var exercises = [Exercise]()
    var preMadeWorkouts = [Workout]()
    var teams = [Team]()
    var allUsers = [String : String]() //Key is userID, value is their full name. There is a key for every userID.
    dynamic var hasFinishedDownloading : Bool = false //Is true when the data has been downloaded at least once from Firebase upon startup. (You can use this to refresh tableViews once data is present. Observe it)
    var allowHasFinishedDownloadingToSet : Bool = true //This value is set to false after we set hasFinishedDownloading to true. That way, we don't repeat and fire a ton of observers
    var userBranchFinishedDownloading : Bool = false {
        didSet {
            if isFinishedDownloading() {
                hasFinishedDownloading = true
                allowHasFinishedDownloadingToSet = false
            }
        }
    }
    var exerciseBranchFinishedDownloading : Bool = false {
        didSet {
            if isFinishedDownloading() {
                hasFinishedDownloading = true
                allowHasFinishedDownloadingToSet = false
            }
        }
    }
    var preMadeBranchFinishedDownloading : Bool = false {
        didSet {
            if isFinishedDownloading() {
                hasFinishedDownloading = true
                allowHasFinishedDownloadingToSet = false
            }
        }
    }
    var teamsBranchFinishedDownloading : Bool = false {
        didSet {
            if isFinishedDownloading() {
                hasFinishedDownloading = true
                allowHasFinishedDownloadingToSet = false
            }
        }
    }
    var workoutBranchFinishedDownloading : Bool = false {
        didSet {
            if isFinishedDownloading() {
                hasFinishedDownloading = true
                allowHasFinishedDownloadingToSet = false
            }
        }
    }
    
    let stringLiterals = Stringleton.sharedInstance
    
    //In case of emergency, break glass...
    func clearData() {
        userID = ""
        coachedTeams = [String]()
        subscribedTeams = [String]()
        displayName = "null"
        firstName = "null"
        lastName = "null"
        completedWorkouts = [Workout]()
        createdExercises = [Exercise]()
        userExercisesTracked = [String : TrackedExercise]()
        userWorkoutCompletionData = WorkoutCompletionObject()
        goals = [Goal]()
        personalWorkouts = [Workout]()
        teamWorkouts = [Workout]()
        userWorkouts = [String]() //An array of workout IDs, from the branch athletikos_data/users/[ID]/workouts
        workouts = [Workout]()
        exercises = [Exercise]()
        preMadeWorkouts = [Workout]()
        teams = [Team]()
        userBranchFinishedDownloading = false
        allowHasFinishedDownloadingToSet = true
        hasFinishedDownloading = false
        exerciseBranchFinishedDownloading = false
        preMadeBranchFinishedDownloading = false
        teamsBranchFinishedDownloading = false
        workoutBranchFinishedDownloading = false
    }
    
    func configure(ID: String) {
        //Watch connectivity Setup
        if WCSession.isSupported() {
            session = WCSession.default()
            session?.delegate = self
            session?.activate()
        }
        
        
        let baseUserRef = FIRDatabase.database().reference(withPath: "\(stringLiterals.userRefRoot)")
        //BRANCH: athletikos_data/users
        //Grabs the name of each user. (Specifically for user in the Coached Teams results view)
        baseUserRef.observe(.value, with: { snapshot in
            self.allUsers.removeAll(keepingCapacity: false)
            for userID in snapshot.children {
                let userSnap = userID as! FIRDataSnapshot
                let tempUserID = userSnap.key
                var tempFirstName = "null"
                var tempLastName = "null"
                for item in userSnap.children {
                    let itemSnap = item as! FIRDataSnapshot
                    
                    if "\(itemSnap.key)" == "first_name"{ // Grabs first name
                        tempFirstName = itemSnap.value as! String
                    }
                    else if "\(itemSnap.key)" == "last_name"{ // Grabs last name
                        tempLastName = itemSnap.value as! String
                    }
                }
                let tempFullName = "\(tempFirstName) \(tempLastName)"
                self.allUsers[tempUserID] = tempFullName
                //print("UserID: \(tempUserID) FirstName: \(tempFirstName) LastName: \(tempLastName)")
            }
        })
        
        let userRef = FIRDatabase.database().reference(withPath: "\(stringLiterals.userRefRoot)\(ID)")
        userID = ID
        //BRANCH: athletikos_data/users/[ID]
        //Grabs all relevant data from the user's database entry.
        userRef.observe(.value, with: { snapshot in
            for item in snapshot.children {
                let itemSnap = item as! FIRDataSnapshot
                
                //Grabs coached teams
                if "\(itemSnap.key)" == "coached_teams" {
                    self.coachedTeams.removeAll(keepingCapacity: false)
                    //BRANCH: athletikos_data/users/[ID]/coached_teams
                    for cItem in itemSnap.children {
                        let cSnap = cItem as! FIRDataSnapshot
                        if let valid = cSnap.value{
                            self.coachedTeams.append("\(valid)")
                        } else {
                            print("invalid coached team. Possibly null")
                        }
                    }
                }
                else if "\(itemSnap.key)" == "subscribed_teams" { //Grabs subscribed teams
                    //BRANCH: athletikos_data/users/[ID]/subscribed_teams
                    self.subscribedTeams.removeAll(keepingCapacity: false)
                    for sItem in itemSnap.children {
                        let sSnap = sItem as! FIRDataSnapshot
                        if let valid = sSnap.value{
                            self.subscribedTeams.append("\(valid)")
                        } else {
                            print("invalid subscribed team. Possibly null")
                        }
                    }
                }
                else if "\(itemSnap.key)" == "display_name"{ // Grabs display name
                    self.displayName = itemSnap.value as! String
                }
                else if "\(itemSnap.key)" == "first_name"{ // Grabs display name
                    self.firstName = itemSnap.value as! String
                }
                else if "\(itemSnap.key)" == "last_name"{ // Grabs display name
                    self.lastName = itemSnap.value as! String
                }
                else if "\(itemSnap.key)" == "completed_workouts" { //The completed workout branch
                    var tempWorkout = Workout() //Create a temp Workout object to store stuff in
                    
                    //BRANCH: athletikos_data/users/[ID]/completed_workouts
                    self.completedWorkouts.removeAll(keepingCapacity: false)
                    for workout in itemSnap.children {
                        let wSnap = workout as! FIRDataSnapshot
                        tempWorkout.workout_id = wSnap.key
                    
                        //BRANCH: athletikos_data/users/[ID]/completed_workouts/...
                        for completedWorkoutItem in wSnap.children {
                            let completedWorkoutsSnap = completedWorkoutItem as! FIRDataSnapshot
                            if "\(completedWorkoutsSnap.key)" == "day_incrementor" {
                                tempWorkout.day_incrementor = completedWorkoutsSnap.value as! Int
                            }
                            else if "\(completedWorkoutsSnap.key)" == "workout_completed" {
                                tempWorkout.workout_completed = completedWorkoutsSnap.value as! Bool
                            }
                            else if "\(completedWorkoutsSnap.key)" == "time_elapsed" {
                                tempWorkout.time_elapsed = completedWorkoutsSnap.value as! Int
                            }
                            else if "\(completedWorkoutsSnap.key)" == "date_completed" {
                                
                                //BRANCH: athletikos_data/users/[ID]/completed_workouts/.../date_completed
                                for date in completedWorkoutsSnap.children {
                                    let dateSnap = date as! FIRDataSnapshot
                                    if "\(dateSnap.key)" == "day" {
                                        tempWorkout.day = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "month" {
                                        tempWorkout.month = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "year" {
                                        tempWorkout.year = dateSnap.value as! Int
                                    }
                                }
                            }
                            
                            else if "\(completedWorkoutsSnap.key)" == "exercises" {
                                
                                //BRANCH: athletikos_data/users/[ID]/completed_workouts/.../exercises
                                for exercise in completedWorkoutsSnap.children {
                                    
                                    let newExercise = Exercise(snapshot: exercise as! FIRDataSnapshot)
                                    tempWorkout.exercises.append(newExercise)
                                }
                            }
                            else {
                                print(self.stringLiterals.firetonKeyError)
                                print("completed workouts branch")
                            }

                            
                            
                        }
                        self.completedWorkouts.append(tempWorkout)
                    }
                    
                }
                else if "\(itemSnap.key)" == "created_exercises" {
                    
                    //BRANCH: athletikos_data/users/[ID]/created_exercises
                    self.createdExercises.removeAll(keepingCapacity: false)
                    for exercise in itemSnap.children {
                        let newExercise = Exercise(snapshot: exercise as! FIRDataSnapshot)
                        self.createdExercises.append(newExercise)
                    }
                    
                }
                else if "\(itemSnap.key)" == "data_tracking" {
                    
                    //BRANCH: athletikos_data/users/[ID]/data_tracking
                    for data in itemSnap.children {
                        let dataSnap = data as! FIRDataSnapshot
                        //bwah
                        if "\(dataSnap.key)" == "exercises_tracked" {
                            self.userExercisesTracked.removeAll(keepingCapacity: false)
                            for exercise in dataSnap.children {
                                let eSnap = exercise as! FIRDataSnapshot
                                let newTrackedExercise = TrackedExercise(snapshot: eSnap, exercise_name: eSnap.key)
                                self.userExercisesTracked = [eSnap.key : newTrackedExercise]
                            }
                        }
                        else if "\(dataSnap.key)" == "workout_completion" {
                            self.userWorkoutCompletionData = WorkoutCompletionObject()
                            self.userWorkoutCompletionData = WorkoutCompletionObject(snapshot: dataSnap)                    
                        }
                        else {
                            print(self.stringLiterals.firetonKeyError)
                            print("data tracking branch")
                        }
                    }
                    
                    //Debug statements
                    /*
                    print("User ID: \(self.userID!)")
                    for (exercise_name, TrackedExercise) in self.userExercisesTracked {
                        print("ExerciseTracked: \(exercise_name)  Total_Reps: \(TrackedExercise.total_reps)")
                    }
                    print("Total_workouts in data tracking: \(self.userWorkoutCompletionData.total_workouts)")
                    print("April 2 num in data tracking: \(self.userWorkoutCompletionData.April["2"])")
                    */
                    //End Debug statements
                }
                else if "\(itemSnap.key)" == "goals" {
                    
                    ////BRANCH: athletikos_data/users/[ID]/goals
                    self.goals.removeAll(keepingCapacity: false)
                    for goal in itemSnap.children {
                        let newGoal = Goal(snapshot: goal as! FIRDataSnapshot)
                        self.goals.append(newGoal)
                    }
                }
                else if "\(itemSnap.key)" == "personal_workouts" {
                    var tempWorkout = Workout() //Create a temp Workout object to store stuff in
                    
                    //BRANCH: athletikos_data/users/[ID]/personal_workouts
                    self.personalWorkouts.removeAll(keepingCapacity: false)
                    for workout in itemSnap.children {
                        let wSnap = workout as! FIRDataSnapshot
                        
                        //BRANCH: athletikos_data/users/[ID]/personal_workouts/...
                        for personalWorkoutItem in wSnap.children {
                            let personalWorkoutSnap = personalWorkoutItem as! FIRDataSnapshot
                            
                            if "\(personalWorkoutSnap.key)" == "date" {
                                
                                //BRANCH: athletikos_data/users/[ID]/personal_workouts/.../date
                                for dateItem in personalWorkoutSnap.children {
                                    let dateSnap = dateItem as! FIRDataSnapshot
                                    if "\(dateSnap.key)" == "day" {
                                        tempWorkout.day = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "month" {
                                        tempWorkout.month = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "year" {
                                        tempWorkout.year = dateSnap.value as! Int
                                    }
                                    else {
                                        print(self.stringLiterals.firetonKeyError)
                                        print("personal workouts branch")
                                    }
                                }
                            }
                            else if "\(personalWorkoutSnap.key)" == "workout_id" {
                                if let valid = personalWorkoutSnap.value{
                                    tempWorkout.workout_id = "\(valid)"
                                } else {
                                    print("invalid workout_id. Possibly null")
                                }

                                
                            }
                            else if "\(personalWorkoutSnap.key)" == "owner" {
                                tempWorkout.owner = personalWorkoutSnap.value as! String
                            }
                            else {
                                print(self.stringLiterals.firetonKeyError)
                                
                                print("personal workouts branch")
                            }
                        }
                        
                        self.personalWorkouts.append(tempWorkout)
                    }
                    
                }
                    
                else if "\(itemSnap.key)" == "team_workouts" {
                    var tempWorkout = Workout() //Create a temp Workout object to store stuff in
                    
                    //BRANCH: athletikos_data/users/[ID]/team_workouts
                    self.teamWorkouts.removeAll(keepingCapacity: false)
                    for workout in itemSnap.children {
                        let wSnap = workout as! FIRDataSnapshot
                        
                        //BRANCH: athletikos_data/users/[ID]/team_workouts/...
                        for teamWorkoutItem in wSnap.children {
                            let teamWorkoutSnap = teamWorkoutItem as! FIRDataSnapshot
                            
                            if "\(teamWorkoutSnap.key)" == "date" {
                                
                                //BRANCH: athletikos_data/users/[ID]/personal_workouts/.../date
                                for dateItem in teamWorkoutSnap.children {
                                    let dateSnap = dateItem as! FIRDataSnapshot
                                    if "\(dateSnap.key)" == "day" {
                                        tempWorkout.day = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "month" {
                                        tempWorkout.month = dateSnap.value as! Int
                                    }
                                    else if "\(dateSnap.key)" == "year" {
                                        tempWorkout.year = dateSnap.value as! Int
                                    }
                                    else {
                                        print(self.stringLiterals.firetonKeyError)
                                        
                                        print("team workouts branch")
                                    }
                                }
                            }
                            else if "\(teamWorkoutSnap.key)" == "workout_id" {
                                
                                tempWorkout.workout_id = "\(teamWorkoutSnap.value!)"
                                //print("Temp Workout ID: \(tempWorkout.workout_id)")
                            }
                            else if "\(teamWorkoutSnap.key)" == "owner" {
                                tempWorkout.owner = "\(teamWorkoutSnap.value)"
                            }
                            else {
                                print(self.stringLiterals.firetonKeyError)
                                print("team workout branch 2")
                            }
                        }
                        
                        self.teamWorkouts.append(tempWorkout)
                    }
                    
                }

                else if "\(itemSnap.key)" == "workouts" {
                    
                    //BRANCH: athletikos_data/users/[ID]/workouts
                    self.userWorkouts.removeAll(keepingCapacity: false)
                    for workoutID in itemSnap.children {
                        let wIDSnap = workoutID as! FIRDataSnapshot
                        if let valid = wIDSnap.value{
                            self.userWorkouts.append("\(valid)")
                        } else {
                            print("invalid workouts. Possibly null")
                        }
                    }
                }
                else {
                    print(self.stringLiterals.firetonKeyError)
                    print("user branch")
                    print(itemSnap.key)
                }
                
            }
            self.retrieveWorkoutData()
            self.userBranchFinishedDownloading = true
        })
        
        //exercises branch
        let exerciseRef = FIRDatabase.database().reference(withPath: stringLiterals.exerciseRefPath)
        
        //BRANCH: athletikos_data/exercises
        exerciseRef.observe(.value, with: { snapshot in
            self.exercises.removeAll(keepingCapacity: false)
            for exercise in snapshot.children {
                let newExercise = Exercise(snapshot: exercise as! FIRDataSnapshot)
                self.exercises.append(newExercise)
            }
            self.exerciseBranchFinishedDownloading = true
        })
        
        //pre_made_workouts branch
        let preMadeRef = FIRDatabase.database().reference(withPath: stringLiterals.preMadeRefPath)
        
        //BRANCH: athletikos_data/pre_made_workouts
        preMadeRef.observe(.value, with: { snapshot in
            self.preMadeWorkouts.removeAll(keepingCapacity: false)
            for workout in snapshot.children {
                let workoutSnap = workout as! FIRDataSnapshot
                
                var tempWorkout = Workout() //Create a temp Workout object to store stuff in
                
                //BRANCH: athletikos_data/per_made_workouts/...
                for detail in workoutSnap.children {
                    let dSnap = detail as! FIRDataSnapshot
                    
                    if "\(dSnap.key)" == "workout_id" {
                        if let valid = dSnap.value{
                            tempWorkout.workout_id = "\(valid)"
                        } else {
                            print("invalid workout id. Possibly null")
                        }
                    }
                    else if "\(dSnap.key)" == "workout_type" {
                        if let valid = dSnap.value{
                            tempWorkout.workout_type = "\(valid)"
                        } else {
                            print("invalid workout type. Possibly null")
                        }
                    }
                    else {
                        print(self.stringLiterals.firetonKeyError)
                        print("pre-made branch")
                    }
                }
                self.preMadeWorkouts.append(tempWorkout)
            }
            self.preMadeBranchFinishedDownloading = true
        })

        //teams branch
        
        let teamRef = FIRDatabase.database().reference(withPath: stringLiterals.teamRefPath)
        //BRANCH: athletikos_data/teams
        teamRef.observe(.value, with: { snapshot in
            self.teams.removeAll(keepingCapacity: false)
            for team in snapshot.children {
                let teamSnap = team as! FIRDataSnapshot
                
                var tempTeam = Team()
                
                tempTeam.team_id = "\(teamSnap.key)"
                //print("Team ID: \(tempTeam.team_id)")
                
                //BRANCH: athletikos_data/teams/...
                for detail in teamSnap.children {
                    let dSnap = detail as! FIRDataSnapshot
                    
                    if "\(dSnap.key)" == "coach_id"{
                        tempTeam.coach_id = dSnap.value as! String
                    }
                    else if "\(dSnap.key)" == "completed_workouts" {
                        
                        //BRANCH: athletikos_data/teams/.../completed_workouts
                        for data in dSnap.children {
                            let dataSnap = data as! FIRDataSnapshot
                            
                            var tempTeamCompletedWorkout = TeamCompletedWorkout()
                            tempTeamCompletedWorkout.workout_id = "\(dataSnap.key)"
                            
                            
                            //BRANCH: athletikos_data/teams/.../completed_workouts/...
                            for user in dataSnap.children {
                                let userSnap = user as! FIRDataSnapshot
                                
                                if "\(userSnap.key)" == "date_completed" {
                                    //BRANCH: athletikos_data/teams/.../completed_workouts/.../date_completed
                                    for dateItem in userSnap.children {
                                        
                                        let dateSnap = dateItem as! FIRDataSnapshot
                                        if "\(dateSnap.key)" == "day" {
                                            tempTeamCompletedWorkout.dayCompleted = dateSnap.value as! Int
                                        }
                                        else if "\(dateSnap.key)" == "month" {
                                            tempTeamCompletedWorkout.monthCompleted = dateSnap.value as! Int
                                        }
                                        else if "\(dateSnap.key)" == "year" {
                                            tempTeamCompletedWorkout.yearCompleted = dateSnap.value as! Int
                                        }
                                        else {
                                            print(self.stringLiterals.firetonKeyError)
                                            print("team completed workouts branch 1 - unrecognized date key")
                                        }
                                        
                                    }
                                }
                                else if "\(userSnap.key)" == "day_incrementor" {
                                    tempTeamCompletedWorkout.day_incrementor = userSnap.value as! Int
                                }
                                else {
                                    var tempWorkout = Workout()
                                
                                    for details in userSnap.children {
                                
                                        let uSnap = details as! FIRDataSnapshot
                                        if "\(uSnap.key)" == "workout_completed" {
                                            tempWorkout.workout_completed = uSnap.value as! Bool
                                        }
                                        else if "\(uSnap.key)" == "exercises" {
                                        
                                            //BRANCH: athletikos_data/teams/.../completed_workouts/.../exercises
                                            for exercise in uSnap.children {
                                            
                                                let newExercise = Exercise(snapshot: exercise as! FIRDataSnapshot)
                                                tempWorkout.exercises.append(newExercise)

                                            }
                                        }
                                        else if "\(uSnap.key)" == "time_elapsed" {
                                            tempWorkout.time_elapsed = uSnap.value as! Int
                                        }
                                        else {
                                            print(self.stringLiterals.firetonKeyError)
                                            print("team completed workouts branch - key unrecognized in completed_workouts/[workout_id]/[user_id]")
                                            print(dSnap.key)
                                        }
                                    }
                                    tempTeamCompletedWorkout.workoutPerUser["\(userSnap.key)"] = tempWorkout //Add the workout to the dict using userID as key
                                    
                                }
                            }
                            tempTeam.allCompletedWorkouts["\(dataSnap.key)"] = tempTeamCompletedWorkout //Add the workout data before moving to the next workout_id
                            
                        }
                    }
                    else if "\(dSnap.key)" == "data_tracking" {
                        
                        //BRANCH: athletikos_data/teams/.../data_tracking
                        for data in dSnap.children {
                            let dataSnap = data as! FIRDataSnapshot
                            
                            if "\(dataSnap.key)" == "exercises_tracked" {
                                for exercise in dataSnap.children {
                                    let eSnap = exercise as! FIRDataSnapshot
                                    let newTrackedExercise = TrackedExercise(snapshot: eSnap, exercise_name: eSnap.key)
                                    tempTeam.exercisesTracked = [eSnap.key : newTrackedExercise]
                                }
                            }
                            else if "\(dataSnap.key)" == "workout_completion" {
                                tempTeam.workoutCompletionData = WorkoutCompletionObject(snapshot: dataSnap)
                                
                            }
                            else {
                                print(self.stringLiterals.firetonKeyError)
                                print("data tracking branch")
                            }
                        }
                    }
                    else if "\(dSnap.key)" == "goals" {
                        
                        //BRANCH: athletikos_data/teams/.../goals
                        for goal in dSnap.children {
                            let newGoal = Goal(snapshot: goal as! FIRDataSnapshot)
                            tempTeam.goals.append(newGoal)
                        }
                    }
                    else if "\(dSnap.key)" == "team_members" {
                        
                        //BRANCH: athletikos_data/teams/.../team_members
                        for user in dSnap.children {
                            let uSnap = user as! FIRDataSnapshot
                            if let valid = uSnap.value{
                                tempTeam.team_members.append("\(valid)")
                            } else {
                                print("invalid team member. Possibly null")
                            }
                        }
                    }
                    else if "\(dSnap.key)" == "team_name" {
                        if let valid = dSnap.value{
                            tempTeam.team_name = "\(valid)"
                        } else {
                            print("invalid team name. Possibly null")
                        }
                    }
                    else if "\(dSnap.key)" == "workouts" {
                        
                        
                        //BRANCH: athletikos_data/teams/.../workouts
                        for workoutID in dSnap.children {
                            var tempWorkout = Workout()
                            
                            let wSnap = workoutID as! FIRDataSnapshot
                            
                            for workoutItem in wSnap.children {
                                let workoutItemSnap = workoutItem as! FIRDataSnapshot
                            
                                if "\(workoutItemSnap.key)" == "workout_id" {
                                    tempWorkout.workout_id = "\(workoutItemSnap.value!)"
                                }
                                else if "\(workoutItemSnap.key)" == "date" {
                                    //BRANCH: athletikos_data/teams/.../completed_workouts/.../date_completed
                                    for dateItem in workoutItemSnap.children {
                                    
                                        let dateSnap = dateItem as! FIRDataSnapshot
                                        if "\(dateSnap.key)" == "day" {
                                            tempWorkout.day = dateSnap.value as! Int
                                        }
                                        else if "\(dateSnap.key)" == "month" {
                                            tempWorkout.month = dateSnap.value as! Int
                                        }
                                        else if "\(dateSnap.key)" == "year" {
                                            tempWorkout.year = dateSnap.value as! Int
                                        }
                                        else {
                                            print(self.stringLiterals.firetonKeyError)
                                            print("team completed workouts branch 2")
                                        }
                                    }
                                }
                            }
                            tempTeam.workouts.append(tempWorkout)
                        }
                        
                    }
                    else {
                        print(self.stringLiterals.firetonKeyError)
                        print("teams branch")
                    }
                    

                    
                }
                /* Debug statement
                for workout in tempTeam.workouts {
                    print("Day: \(workout.day) - Month: \(workout.month) - Year: \(workout.year) - workoutID: \(workout.workout_id)")
                }
 */
                self.teams.append(tempTeam)
                
                //Debug statements
                /*
                print("TEAM ID: \(tempTeam.team_id)")
                for (exercise_name, TrackedExercise) in tempTeam.exercisesTracked {
                    print("ExerciseTracked: \(exercise_name)  Total_Reps: \(TrackedExercise.total_reps)")
                }
                print("Total_workouts in data tracking: \(tempTeam.workoutCompletionData.total_workouts)")
                print("April 2 num in data tracking: \(tempTeam.workoutCompletionData.April["2"])")
                 */
                //End Debug statements
            }
            self.teamsBranchFinishedDownloading = true
        })
    }
    
    //This function pulls only the relevant workout data. Should only be called once (at startup)
    func retrieveWorkoutData() {
        
        //This will hold the workout IDs for the workouts the user can perform.
        var relevantWorkoutIDs = [String]()
        
        for workout in completedWorkouts {
            relevantWorkoutIDs.append(workout.workout_id)
        }
        for workout in personalWorkouts {
            relevantWorkoutIDs.append(workout.workout_id)
        }
        for workout in teamWorkouts {
            relevantWorkoutIDs.append(workout.workout_id)
        }
        relevantWorkoutIDs.append(contentsOf: userWorkouts)
        
        //Debug statement
        /*
        for ID in relevantWorkoutIDs {
            print("RELEVANT WORKOUT IDs: \(ID)")
        }
        */
        
        
        let workoutRef = FIRDatabase.database().reference(withPath: stringLiterals.workoutRefPath)
        
        //BRANCH: athletikos_data/workouts
        workoutRef.observe(.value, with: { snapshot in
            self.workouts.removeAll(keepingCapacity: false)
            for item in snapshot.children {
                let itemSnap = item as! FIRDataSnapshot
                
                var tempWorkout = Workout() //Create a temp Workout object to store stuff in
                
                //We only pull workout data if the workout is relevant to the user. By 'relevant,' we mean a workout that the user has added to his list or that a coach of his added.
                if relevantWorkoutIDs.contains(itemSnap.key) {
                    //print(">>>>>>>>>ISILDOR! \(itemSnap.key)")
                
                    tempWorkout.workout_id = itemSnap.key
                
                    //BRANCH: athletikos_data/workouts/...
                    for workout in itemSnap.children {
                        let workoutSnap = workout as! FIRDataSnapshot
                    
                        if "\(workoutSnap.key)" == "workout_name" {
                            tempWorkout.workout_name = "\(workoutSnap.value!)"
                        }
                        
                        else if "\(workoutSnap.key)" == "exercises" {
                        
                            //BRANCH: athletikos_data/workouts/.../exercises
                            for exercise in workoutSnap.children {
                            
                                let newExercise = Exercise(snapshot: exercise as! FIRDataSnapshot)
                                tempWorkout.exercises.append(newExercise)
                            }
                        }
                        else {
                            print(self.stringLiterals.firetonKeyError)
                            print("workout branch")
                        }
                    
                    }
                    self.workouts.append(tempWorkout)
                }
                
            }
            
            /*
            for workout in self.workouts {
                print("WORKOUTS THAT WERE SAVED: \(workout.workout_id)")
            }
            */
            
            self.workoutBranchFinishedDownloading = true
        })

    }
    
    //Returns true if all branches of the database have been downloaded at least once.
    func isFinishedDownloading() -> Bool {
        
        return (userBranchFinishedDownloading && exerciseBranchFinishedDownloading && preMadeBranchFinishedDownloading && teamsBranchFinishedDownloading && workoutBranchFinishedDownloading && allowHasFinishedDownloadingToSet)
    }
    
    //Give this function a workout_id and it returns the workout with that ID.
    func findWorkout(withID: String) -> Workout {
        for workout in workouts {
            if workout.workout_id == withID {
                return workout
            }
        }
        print(stringLiterals.firetonFindWorkoutError)
        return Workout()
    }
    
    //Give this function a team_id and it returns the team object with that ID.
    func findTeam(withID: String) -> Team {
        for team in teams {
            if team.team_id == withID {
                return team
            }
        }
        print("Fireton findTeam Error: Couldn't find the team")
        return Team()
    }
    
    
    //Make this a Singleton class
    static let sharedInstance: Fireton = {
        let instance = Fireton()
        return instance
    }()
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        } else {
            print("Connection Succeeded")
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session Inactive")
    }
    
    //Watch Delegate Interaface
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session Deactivated")
    }
}
