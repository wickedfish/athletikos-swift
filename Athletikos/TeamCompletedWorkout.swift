//
//  TeamCompletedWorkout.swift
//  Athletikos
//
//  Created by Speedy Gonzalez on 4/7/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import Firebase

struct TeamCompletedWorkout {
    var workout_id : String
    var workoutPerUser = [String : Workout]() //Key is user ID, value is the workout they completed.
    var dayCompleted : Int
    var monthCompleted : Int
    var yearCompleted : Int
    var day_incrementor : Int
    
    init() {
        workout_id = "null"
        dayCompleted = 0
        monthCompleted = 0
        yearCompleted = 0
        day_incrementor = 0
    }
}
