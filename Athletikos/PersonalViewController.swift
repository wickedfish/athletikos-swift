//
//  PersonalViewController.swift
//  Athletikos
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Charts
import Firebase

private let reuseIdentifier = "PersonalGoalCell"


class PersonalViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate, UITableViewDataSource, ChartViewDelegate  {
    
    @IBOutlet weak var goalsCollection: UICollectionView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var workoutsLabel: UILabel!
    @IBOutlet weak var workoutsTable: UITableView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var chartSegControl: UISegmentedControl!
    @IBOutlet weak var chartLabel : UILabel!
    
    @IBAction func chartSegControlChange(){
        if(chartSegControl.selectedSegmentIndex == 0){
            loadMonthChart()
            loadMonth = false
        } else{
            loadWeekChart()
            loadMonth = true
        }
    }
    
    var row = 0
    
    let days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    let week = ["","S","M","T","W","T","F","S"]
    
    let dayCompleted = Fireton.sharedInstance.userWorkoutCompletionData.monthData
    var workoutsCompleted = [Int]()
    
    let dayCompletedWeekly = Fireton.sharedInstance.userWorkoutCompletionData.week
    var workoutsCompletedWeekly = [Int]()
    
    let userData = Fireton.sharedInstance

    let style = Styleton.sharedInstance
    
    var format : IAxisValueFormatter!
    
    var loadMonth = true
    
    var personalRoutines = [(String , Date, DateComponents)]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Styling
        progressLabel.textColor = style.white
        goalsLabel.textColor = style.white
        workoutsLabel.textColor = style.white
        view.backgroundColor = style.blue
        
        workoutsLabel.text = "Workouts"
        
        workoutsTable.layer.borderWidth = 3
        workoutsTable.layer.borderColor = style.blue.cgColor
        workoutsTable.layer.cornerRadius = 10
        goalsCollection.layer.borderWidth = 3
        goalsCollection.layer.borderColor = style.blue.cgColor
        goalsCollection.layer.cornerRadius = 10
        lineChartView.layer.cornerRadius = 30
        
        for i in 1 ..< 31 {
            if(dayCompleted[String(i)] != nil){
                workoutsCompleted.append(dayCompleted[String(i)]!)
            }
        }
        
        for i in 1 ..< week.count+1 {
            if(dayCompletedWeekly[String(i)] != nil){
                workoutsCompletedWeekly.append(dayCompletedWeekly[String(i)]!)
            }
        }
        
        
        //speedy's line?
        userData.addObserver(self, forKeyPath: "hasFinishedDownloading", options: .new, context: nil)
        
        //loads chart
        loadMonthChart()
        loadMonth = false
        
        //saves format for month chart display
        format = self.lineChartView.xAxis.valueFormatter
        
        //creates array of worjout name and dates for the workout then sorts based on date
        loadWorkoutData()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {}

    
    //example function to set chart data
    func setChartData(day : [Int]) {
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        
        if(loadMonth){
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< 31 {
                if(dayCompleted.keys.contains(String(i))){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(workoutsCompleted.prefix(yVals1.count).reduce(0, +))))
                }
            }
        } else{
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< week.count+1 {
                if(dayCompletedWeekly.keys.contains(String(i))){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(workoutsCompletedWeekly.prefix(yVals1.count).reduce(0, +))))
                }
            }
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "First Set")
        set1.axisDependency = .left // Line will correlate with left axis values
        set1.setColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 2.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)
        set1.highlightColor = UIColor.white
        set1.drawCircleHoleEnabled = false
        
        //3 - create an array to store our LineChartDataSets
        var dataSets = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData!
        if(loadMonth){
            data = LineChartData(dataSets: dataSets)
            data.setValueTextColor(UIColor.white)
        } else{
        data = LineChartData(dataSets: dataSets)
        data.setValueTextColor(UIColor.white)
        }
        
        //5 - finally set our data
        self.lineChartView.data = data
 
    }
    //loads chart
    func loadMonthChart() {
        //if true loads month data
        self.lineChartView.delegate = self
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 6
        self.lineChartView.leftAxis.axisMaximum = 30
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 6
        self.lineChartView.xAxis.axisMaximum = 30
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.drawBordersEnabled = true
        self.lineChartView.xAxis.valueFormatter = format
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        self.lineChartView.highlightPerTapEnabled = false
        self.lineChartView.doubleTapToZoomEnabled = false
        
        setChartData(day: days)
    }
    
    func loadWeekChart() {
        self.lineChartView.delegate = self
        //self.lineChartView.chartDescription?.text = "Tap node for details"
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 7
        self.lineChartView.leftAxis.axisMaximum = 7
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 7
        self.lineChartView.xAxis.axisMaximum = 7
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:week)
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        
        setChartData(day: days)
    }
    
    //creates array of worjout name and dates for the workout then sorts based on date
    func loadWorkoutData(){
        for routine in userData.personalWorkouts{
            for workout in userData.workouts{
                if routine.workout_id == workout.workout_id{
                    var workoutDate = DateComponents()
                    workoutDate.day = routine.day
                    workoutDate.month = routine.month
                    workoutDate.year = routine.year
                    
                    let cal = Calendar.current
                    
                    personalRoutines.append((workout.workout_name, cal.date(from: workoutDate)!,workoutDate))
                }
            }
        }
        personalRoutines.sort{$0.1 < $1.1}
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if(userData.goals.count == 1){
            let totalCellWidth =  169 * userData.goals.count
            let totalSpacingWidth = 10 * (userData.goals.count - 1)
        
            let leftInset = ((self.goalsCollection.frame.width/2) - CGFloat(totalCellWidth + totalSpacingWidth) / 2)
            let rightInset = leftInset
        
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        } else if(userData.goals.count == 2){
            let totalCellWidth =  169 * userData.goals.count
            let totalSpacingWidth = 10 * (userData.goals.count - 1)
            
            let leftInset = ((self.goalsCollection.frame.width/2) - CGFloat(Int(totalCellWidth) + totalSpacingWidth)/2)
            let rightInset = leftInset
            
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userData.goals.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GoalCollectionCell
        
        let currentValue = userData.goals[(indexPath as NSIndexPath).row].current_val
        let target = userData.goals[(indexPath as NSIndexPath).row].target_val
        
        cell.goalLabel.text = userData.goals[(indexPath as NSIndexPath).row].tracker_id
        
        if(target > currentValue){
            cell.showProgress(goal: Double(target), progress: Double(currentValue))
        } else {
            cell.showProgress(goal: Double(target), progress: Double(1.0))
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(userData.personalWorkouts.count > 4){
            return 4
        } else {
            return userData.personalWorkouts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personalCell", for: indexPath) as! PersonalWorkoutCell
        
        cell.workoutLabel.text = personalRoutines[(indexPath as NSIndexPath).row].0
        var workoutDateComponents = personalRoutines[(indexPath as NSIndexPath).row].2
        
        let dateFormatter: DateFormatter = DateFormatter()
        let months = dateFormatter.shortMonthSymbols
        let monthSymbol = months?[workoutDateComponents.month!-1]
        let cal = Calendar.current
        
        if(cal.isDateInToday(personalRoutines[(indexPath as NSIndexPath).row].1) ){
            cell.dateLabel.text = "Today"
        }else {
            cell.dateLabel.text = "\(monthSymbol!) \(workoutDateComponents.day!)"

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row

        return indexPath
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Should check if segue is to the In-Progress Workout Page
        if segue.identifier == "toInProgressFromPersonalPage" {
            //self.navigationController?.isNavigationBarHidden = false

            let dest = segue.destination as! WorkoutVC
            dest.workout_id = userData.personalWorkouts[row].workout_id
            dest.isPersonalWorkout = true
        }
    }
    
    deinit {
        userData.removeObserver(self, forKeyPath: "hasFinishedDownloading")
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

