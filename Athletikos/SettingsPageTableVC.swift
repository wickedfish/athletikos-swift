//
//  SettingPageTableVC.swift
//  Athletikos
//
//  Created by Andrew Henry on 2/7/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SettingsPageTableVC: UITableViewController{

    var defaults = UserDefaults.standard
    
    let fire = Fireton.sharedInstance
    
    let style = Styleton.sharedInstance
    
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var PersonalCell: SettingsPageCell!
    @IBOutlet weak var MyTeamsCell: SettingsPageCell!
    @IBOutlet weak var CoachedTeamsCell: SettingsPageCell!
    @IBOutlet weak var AllNotifCell: SettingsPageCell!
    @IBOutlet weak var TeamNotifCell: SettingsPageCell!
    @IBOutlet weak var PersonalNotifCell: SettingsPageCell!
    @IBOutlet weak var NoNotifCell: SettingsPageCell!
    @IBOutlet weak var signOutButton: UIButton!

    @IBAction func PersonalSwitchTap(sender : UISwitch){
        if(sender.isOn){
            MyTeamsCell.toggle.setOn(false, animated: true)
            CoachedTeamsCell.toggle.setOn(false, animated: true)
            defaults.set("My Page", forKey: "homePage")        }
        else{
            PersonalCell.toggle.setOn(true, animated: false)
        }
    }
    @IBAction func MyTeamsSwitchTap(sender : UISwitch){
        if(sender.isOn){
            PersonalCell.toggle.setOn(false, animated: true)
            CoachedTeamsCell.toggle.setOn(false, animated: true)
            defaults.set("My Teams", forKey: "homePage")
        } else{
            MyTeamsCell.toggle.setOn(true, animated: false)
        }

    }
    @IBAction func CoachedTeamSwitchTap(sender : UISwitch){
        if(sender.isOn){
            PersonalCell.toggle.setOn(false, animated: true)
            MyTeamsCell.toggle.setOn(false, animated: true)
            defaults.set("Coached Teams", forKey: "homePage")
        } else{
            CoachedTeamsCell.toggle.setOn(true, animated: false)
        }

    }
    @IBAction func AllNotifSwitchTap(sender : UISwitch){
        if(sender.isOn){
            TeamNotifCell.toggle.setOn(false, animated: true)
            NoNotifCell.toggle.setOn(false, animated: true)
            PersonalNotifCell.toggle.setOn(false, animated: true)
            defaults.set("all", forKey: "notifications")
        } else{
            AllNotifCell.toggle.setOn(true, animated: false)
        }

    }
    @IBAction func TeamNotifSwitchTap(sender : UISwitch){
        if(sender.isOn){
            AllNotifCell.toggle.setOn(false, animated: true)
            NoNotifCell.toggle.setOn(false, animated: true)
            PersonalNotifCell.toggle.setOn(false, animated: true)
            defaults.set("team", forKey: "notifications")
        } else{
            TeamNotifCell.toggle.setOn(true, animated: false)
        }

    }
    @IBAction func PersonalNotifSwitchTap(sender : UISwitch){
        if(sender.isOn){
            AllNotifCell.toggle.setOn(false, animated: true)
            NoNotifCell.toggle.setOn(false, animated: true)
            TeamNotifCell.toggle.setOn(false, animated: true)
            defaults.set("personal", forKey: "notifications")
        } else{
            PersonalNotifCell.toggle.setOn(true, animated: false)
        }

    }
    @IBAction func NoNotifSwitchTap(sender : UISwitch){
        if(sender.isOn){
            TeamNotifCell.toggle.setOn(false, animated: true)
            AllNotifCell.toggle.setOn(false, animated: true)
            PersonalNotifCell.toggle.setOn(false, animated: true)
            defaults.set("none", forKey: "notifications")
        } else{
            NoNotifCell.toggle.setOn(true, animated: false)
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Style        
        signOutButton.layer.cornerRadius = 5
        editProfileButton.layer.cornerRadius = 5
        
        self.title = "Settings"
        
        PersonalCell.selectionStyle = UITableViewCellSelectionStyle.none
        MyTeamsCell.selectionStyle = UITableViewCellSelectionStyle.none
        CoachedTeamsCell.selectionStyle = UITableViewCellSelectionStyle.none
        AllNotifCell.selectionStyle = UITableViewCellSelectionStyle.none
        TeamNotifCell.selectionStyle = UITableViewCellSelectionStyle.none
        PersonalNotifCell.selectionStyle = UITableViewCellSelectionStyle.none
        NoNotifCell.selectionStyle = UITableViewCellSelectionStyle.none
        
        tableView.separatorStyle = .none
        
        //default page
        //sets default if first time being loaded, or loads users preference
        if(defaults.object(forKey: "homePage") == nil){
            defaults.set("My Page", forKey: "homePage")
            MyTeamsCell.toggle.setOn(false, animated: true)
            CoachedTeamsCell.toggle.setOn(false, animated: true)
            PersonalCell.toggle.setOn(true, animated: true)
        } else {
            switch((defaults.object(forKey: "homePage") as! String)){
                case "My Page":
                    MyTeamsCell.toggle.setOn(false, animated: true)
                    CoachedTeamsCell.toggle.setOn(false, animated: true)
                    PersonalCell.toggle.setOn(true, animated: true)
                case "My Teams":
                    PersonalCell.toggle.setOn(false, animated: true)
                    CoachedTeamsCell.toggle.setOn(false, animated: true)
                    MyTeamsCell.toggle.setOn(true, animated: true)
                case "Coached Teams":
                    PersonalCell.toggle.setOn(false, animated: true)
                    CoachedTeamsCell.toggle.setOn(true, animated: true)
                    MyTeamsCell.toggle.setOn(false, animated: true)
                default: break
            }
        }
        
        //notfications
        //sets default if first time being loaded, or loads users preference
        if(defaults.object(forKey: "notifications") == nil){
            defaults.set("all", forKey: "notifications")
            PersonalNotifCell.toggle.setOn(false, animated: true)
            AllNotifCell.toggle.setOn(true, animated: true)
            NoNotifCell.toggle.setOn(false, animated: true)
            TeamNotifCell.toggle.setOn(false, animated: true)
        } else {
            switch((defaults.object(forKey: "notifications") as! String)){
            case "all":
                PersonalNotifCell.toggle.setOn(false, animated: true)
                AllNotifCell.toggle.setOn(true, animated: true)
                NoNotifCell.toggle.setOn(false, animated: true)
                TeamNotifCell.toggle.setOn(false, animated: true)
            case "team":
                PersonalNotifCell.toggle.setOn(false, animated: true)
                AllNotifCell.toggle.setOn(false, animated: true)
                NoNotifCell.toggle.setOn(false, animated: true)
                TeamNotifCell.toggle.setOn(true, animated: true)
            case "personal":
                PersonalNotifCell.toggle.setOn(true, animated: true)
                AllNotifCell.toggle.setOn(false, animated: true)
                NoNotifCell.toggle.setOn(false, animated: true)
                TeamNotifCell.toggle.setOn(false, animated: true)
            case "none":
                PersonalNotifCell.toggle.setOn(false, animated: true)
                AllNotifCell.toggle.setOn(false, animated: true)
                NoNotifCell.toggle.setOn(true, animated: true)
                TeamNotifCell.toggle.setOn(false, animated: true)
            default: break
            }
        }
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signOutButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Sign-Out", message: "Confirm Sign Out", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title:"Yes", style: .cancel, handler:  { action in self.performSegue(withIdentifier: "signOutFromSettings", sender: self)}))
        
        alertController.addAction(UIAlertAction(title:"No", style: .destructive, handler:nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func editProfilePressed(_ sender: Any) {
        performSegue(withIdentifier: "editProfileFromSettings", sender: nil)
    }
    
    @IBAction func unwindToSettingsPage(segue: UIStoryboardSegue) {
        navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editProfileFromSettings" {
            let destination = segue.destination as! profileSetupVC
            destination.sentFrom = "settings"
        }
    }
    //look into userdeafaults
    
    /*
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        row = (indexPath as NSIndexPath).row
        section = (indexPath as NSIndexPath).section
        return indexPath
    }
  */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
