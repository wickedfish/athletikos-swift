//
//  Styleton.swift
//  Athletikos
//
//  Created by Daniel Porter on 4/2/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

//For all the styling of the app

import Foundation



class Styleton: NSObject {
    //Color pallete for the app
    let logoName = "helmetLogo.png"
    
    let blue = UIColor(red:0.251, green:0.600, blue:1.0, alpha:1.0) //#colorLiteral(red: 0.2509803922, green: 0.6, blue: 1, alpha: 1)
    let green = UIColor(red: 0.471, green: 0.961, blue: 0.0, alpha: 1.0) //#colorLiteral(red: 0.4705882353, green: 0.9607843137, blue: 0, alpha: 1)
    let purple = UIColor(red: 0.620, green: 0.031, blue: 0.875, alpha: 1.0) //#colorLiteral(red: 0.6196078431, green: 0.03137254902, blue: 0.8745098039, alpha: 1)
    let red = UIColor(red: 0.929, green: 0.0, blue: 0.302, alpha: 1.0) //#colorLiteral(red: 0.8941176471, green: 0, blue: 0.3019607843, alpha: 1)
    let white = UIColor .white
    let gray = UIColor .lightText
    
    //Make this a Singleton class
    static let sharedInstance: Styleton = {
        let instance = Styleton()
        return instance
    }()
}
