//
//  SubscribedViewController.swift
//  Athletikos
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Firebase

class SubscribedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var JoinButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var userData = Fireton.sharedInstance
    var teamName = ""
    var row = 0
    var iPath = IndexPath()
    
    let style = Styleton.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = style.blue
        tableView.tableFooterView = UIView()
        tableView.layer.borderWidth = 3
        tableView.layer.borderColor = style.blue.cgColor
        tableView.layer.cornerRadius = 10
        
        JoinButton.layer.cornerRadius = 10
        JoinButton.layer.borderColor = style.blue.cgColor
        JoinButton.layer.borderWidth = 3
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //creates alert with textfield and presents it to the user
    @IBAction func joinButton(sender: UIButton){
        let alert = UIAlertController(title: "Join", message: "Enter your team code", preferredStyle: .alert)
        
        //creates join button on alert
        //deal with data handling here
        let joinAction = UIAlertAction(title: "Join", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0]
            var teamFound = false
            
            for team in self.userData.teams {
                if(team.team_id == textField.text && !(self.userData.subscribedTeams.contains(team.team_id))){
                    teamFound = true
                    //creates reference to users subscribed teams
                    let ref = FIRDatabase.database().reference(withPath: "athletikos_data/users/\(self.userData.userID!)/subscribed_teams")
                    ref.child(team.team_id).setValue(team.team_id)
                    
                    self.userData.subscribedTeams.append(team.team_id)
                    
                    self.tableView.reloadData()
                    
                    
                    
                }
            }
            //presents invlid code alert
            if(teamFound == false){
                let alertInvalidID = UIAlertController(title: "Invalid Code", message: "Please Enter a Valid Team Code", preferredStyle: .alert)
                let cancelInvalid = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in })
                
                //adds buttons and presents alert
                alertInvalidID.addAction(cancelInvalid)
                self.present(alertInvalidID, animated: true, completion: nil)
            }
        })
        
        //cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        //adds the text field
        alert.addTextField(configurationHandler: { (alertTextField: UITextField) in
            alertTextField.keyboardAppearance = .dark
            alertTextField.keyboardType = .default
            alertTextField.autocorrectionType = .no
            alertTextField.placeholder = "Type Here"
            alertTextField.clearButtonMode = .whileEditing
        })
        
        //add buttons and present alert
        alert.addAction(joinAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  userData.subscribedTeams.count// your number of cell here
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SubscribedTeamCell
        
        for team in userData.teams{
            if team.team_id == userData.subscribedTeams[(indexPath as NSIndexPath).row]{
                cell.teamLabel.text = team.team_name
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        row = (indexPath as NSIndexPath).row
        iPath = indexPath
        return indexPath
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = (segue.destination as! IndividualSubscribedVC)
        dest.teamID = userData.subscribedTeams[row]
        dest.teamName = (tableView.cellForRow(at: iPath) as! SubscribedTeamCell).teamLabel.text!
    }
    

}
