//
//  TapExDetailsVC.swift
//  Athletikos
//
//  Created by Joshua Blasdell on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Charts

private let reuseIdentifier = "DetailGoalCell"

class TapExDetailsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ChartViewDelegate {
    
    var exercise_name = "null"  //Speedy: This will be set by the in-progress workout page. This is how you'll know which exercise to load details for.
    var exercise_row : Int = 0
    var prog_image: UIImage?
    let fire = Fireton.sharedInstance
    var exImage : URL?
    //Line chart variables
    var dayCompleted = Fireton.sharedInstance.userWorkoutCompletionData.monthData.values
    var dayCompletedWeekly = Fireton.sharedInstance.userWorkoutCompletionData.week.values
    let days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    let week = ["","S","M","T","W","T","F","S"]
    var descriptionText: String!
    var loadMonth = true
    var format : IAxisValueFormatter!
    //variables to pass the reps and weight to the dials
    var startingReps: Int!
    var startingWeight: Int!
    
    @IBOutlet weak var chartSegControl: UISegmentedControl!
    
    @IBOutlet weak var weightWell: UIView!
    @IBOutlet weak var dialsWell: UIView!
    @IBOutlet weak var progression: UIImageView!
    @IBOutlet weak var exDescription: UITextView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var dialOne: BWCircularSliderView!
    @IBOutlet weak var dialTwo: BWCircularSliderView!
    
    @IBAction func chartSegControlChange(){
        if(chartSegControl.selectedSegmentIndex == 0){
            loadMonthChart()
            loadMonth = false
        } else{
            loadWeekChart()
            loadMonth = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //startingReps = startingReps*12
        //startingWeight = Int(Double(startingWeight)/1.4)
        
        self.navigationItem.title = exercise_name
        
        dialOne.isRepsDial = true //Speedy's contribution. This allows one slider to display reps in increments of 1.
        dialOne.currentValue = startingReps
        
        dialTwo.currentValue = startingWeight
        
        view.backgroundColor = style.blue
        
        exDescription.layer.cornerRadius = 10
        exDescription.layer.borderWidth = 3
        exDescription.layer.borderColor = style.blue.cgColor
        dialsWell.layer.cornerRadius = 10
        dialsWell.layer.borderWidth = 3
        dialsWell.layer.borderColor = style.blue.cgColor
        weightWell.layer.cornerRadius = 10
        weightWell.layer.borderWidth = 3
        weightWell.layer.borderColor = style.blue.cgColor
        
        //loads chart
        loadMonthChart()
        loadMonth = false
        
        format = self.lineChartView.xAxis.valueFormatter
        
        //load exercise chartDescription
        for Exercise in fire.exercises{
            if Exercise.exercise_name == self.exercise_name {
                self.descriptionText = Exercise.exercise_description
            }
        }
        exDescription.text = descriptionText
        
        //Load image progression
        for Exercise in fire.exercises{
            if Exercise.exercise_name == self.exercise_name {
                self.exImage = URL(string: Exercise.image_url)
            }else{
                self.exImage = URL(string: "https://cdn.pixabay.com/photo/2016/08/31/22/21/dumbbells-1634750_960_720.jpg")
            }
        }
        
            DispatchQueue.global(qos: .userInitiated).async {
            let imageData = NSData(contentsOf: self.exImage!)
            //All UI operations has to run on main thread.
            DispatchQueue.main.async {
                if imageData != nil {
                    self.prog_image = UIImage(data: imageData as! Data)
                    self.progression.image = self.prog_image
                    self.progression.sizeToFit()
                } else {
                    self.prog_image = #imageLiteral(resourceName: "helmetLogo")
                    self.progression.image = self.prog_image
                    self.progression.sizeToFit()
                }
            }
        }
        
        self.navigationItem.hidesBackButton = true
        /*
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back",
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(backButtonDidTouch))
 */
        
    }
    
    func backButtonDidTouch() {
        performSegue(withIdentifier: "toPersonalPage", sender: nil)
    }
    
    func setChartData(day : [Int]) {
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        
        if(loadMonth){
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< 31 {
                if(dayCompleted.contains(i)){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(yVals1.count)))
                }
            }
        } else{
            yVals1.append(ChartDataEntry(x: 0.0, y: 0.0))
            for i in 1 ..< week.count+1 {
                if(dayCompletedWeekly.contains(i)){
                    yVals1.append(ChartDataEntry(x: Double(i), y: Double(yVals1.count)))
                }
            }
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "First Set")
        set1.axisDependency = .left // Line will correlate with left axis values
        set1.setColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 2.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.init(red: CGFloat(64) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)
        set1.highlightColor = UIColor.white
        set1.drawCircleHoleEnabled = false
        
        //3 - create an array to store our LineChartDataSets
        var dataSets = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData!
        if(loadMonth){
            data = LineChartData(dataSets: dataSets)
            data.setValueTextColor(UIColor.white)
        } else{
            data = LineChartData(dataSets: dataSets)
            data.setValueTextColor(UIColor.white)
        }
        
        //5 - finally set our data
        self.lineChartView.data = data
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GoalCollectionCell
        
        // Configure the cell
        cell.goalLabel.text = "Hello"
        cell.showProgress(goal: 3, progress: 2)
        
        return cell
    }
    
    //loads chart
    func loadMonthChart() {
        //if true loads month data
        self.lineChartView.delegate = self
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 6
        self.lineChartView.leftAxis.axisMaximum = 30
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 6
        self.lineChartView.xAxis.axisMaximum = 30
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.drawBordersEnabled = true
        self.lineChartView.xAxis.valueFormatter = format
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        self.lineChartView.highlightPerTapEnabled = false
        self.lineChartView.doubleTapToZoomEnabled = false
        
        setChartData(day: days)
    }
    
    func loadWeekChart() {
        self.lineChartView.delegate = self
        //self.lineChartView.chartDescription?.text = "Tap node for details"
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        self.lineChartView.noDataText = "No data provided"
        
        self.lineChartView.leftAxis.labelCount = 7
        self.lineChartView.leftAxis.axisMaximum = 7
        self.lineChartView.leftAxis.axisMinimum = 0
        self.lineChartView.leftAxis.drawGridLinesEnabled = false
        
        self.lineChartView.xAxis.labelCount = 7
        self.lineChartView.xAxis.axisMaximum = 7
        self.lineChartView.xAxis.axisMinimum = 0
        self.lineChartView.xAxis.drawGridLinesEnabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:week)
        
        self.lineChartView.rightAxis.enabled = false
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.legend.enabled = false
        self.lineChartView.scaleXEnabled = false
        self.lineChartView.scaleYEnabled = false
        
        setChartData(day: days)
    }

}
