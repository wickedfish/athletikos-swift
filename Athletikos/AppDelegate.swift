//
//  AppDelegate.swift
//  Athletikos
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate{
    var window: UIWindow?
    
    let fire = Fireton.sharedInstance

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("wil present")
        completionHandler([.alert,.sound])
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //local notification stuff
        let center = UNUserNotificationCenter.current()
        let category = UNNotificationCategory(identifier: "testCat", actions: [], intentIdentifiers: [], options: [])
        center.setNotificationCategories([category])
        
        
        let options: UNAuthorizationOptions = [.alert, .sound]
        center.requestAuthorization(options: options, completionHandler: {
            (granted, error) in
            if !granted {
                print("Local Notification Error")
            }
        })
        //This line allows the database to work offline. No issues if connection is lost. Additionally, offline updates to the database will apply once a connection is made.
        
        center.delegate = self
        
        BuddyBuildSDK.setup()

        
        //Configures Firebase stuff. Do not mess with this.
        FIRApp.configure()
        FIRDatabase.database().persistenceEnabled = true
        
        //Hard-coded, for testing
//        fire.configure(ID: "VCJKf3ZaSUWPiFt5tC9MKEglA7J2")
//        print("App Delegate: \(fire.displayName)")

                
        // Override point for customization after application launchz
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

