//
//  PastResultsCell.swift
//  Athletikos
//
//  Created by Joshua Blasdell on 2/14/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class PastResultsCell: UITableViewCell {
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var ResultImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
