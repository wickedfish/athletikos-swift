//
//  EmbeddedViewController.swift
//  Athletikos
//
//  Created by Andrew Henry on 2/3/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import UIKit

class EmbeddedViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var settingButton: UIBarButtonItem!
    
    var MainPageViewController: MainPageViewController? {
        didSet {
            MainPageViewController?.mainPageDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //creates the settings button
        settingButton.title = "\u{2699}"
        
        //sets title of initial screen
        if(UserDefaults.standard.object(forKey: "homePage") != nil){
            self.navigationController?.navigationBar.topItem?.title = (UserDefaults.standard.object(forKey: "homePage")) as? String
        }else {
            self.navigationController?.navigationBar.topItem?.title = "My Page"
        }
        
        
        pageControl.addTarget(self, action: #selector(EmbeddedViewController.didChangePageControlValue), for: .valueChanged)
    }
    
    //place holder for the selector of the setting button
    func doesNothing(){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let MainPageViewController = segue.destination as? MainPageViewController {
            self.MainPageViewController = MainPageViewController
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didChangePageControlValue() {
        MainPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EmbeddedViewController: MainPageViewControllerDelegate {
    
    func MainPageViewController(_ MainPageViewController: MainPageViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func MainPageViewController(_ MainPageViewController: MainPageViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
