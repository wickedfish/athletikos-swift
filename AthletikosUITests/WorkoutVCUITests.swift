//
//  WorkoutVCUITests.swift
//  Athletikos
//
//  Created by Zach on 4/19/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class WorkoutVCUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testStartBtnExists(){
        
        let app = XCUIApplication()
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        XCTAssert(app.buttons["Start Workout"].exists)
        
    }
    
    func testStartBtnTap(){
        
        let app = XCUIApplication()
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        app.buttons["Start Workout"].tap()
        XCTAssert(XCUIApplication().navigationBars["still not leg day"].buttons["Rest"].exists)
        
    }
    func testDetails(){
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["still not leg day"].tap()
        tablesQuery.staticTexts["bench press"].press(forDuration: 1.9);
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 2).tap()
        XCTAssert(XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 2).exists)
    }
    
}
