//
//  UITestCoachedViewController.swift
//  Athletikos
//
//  Created by Nate Henry on 4/19/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class UITestCoachedViewController: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSelectedTeam(){
        let app = XCUIApplication()
        
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).swipeRight()
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Completed Workouts"].exists)
    }
    
}
