//
//  PersonalVCUITests.swift
//  Athletikos
//
//  Created by Daniel Porter on 4/18/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

//Should be logged in under natechenry111@gmail.com for these tests

import XCTest

class PersonalVCUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAllElementsExist() {
        sleep(4)
        
        XCTAssert(app.tables.element(boundBy: 0).exists)
        XCTAssert(app.collectionViews.element(boundBy: 0).exists)
        XCTAssert(XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.exists)
        
    }
    
    func testTapSettings() {
        sleep(4)
        
        app.navigationBars["My Page"].buttons["⚙"].tap()
        app.navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars["My Page"].staticTexts["My Page"].exists)
    }
    
    func testSwipeLeft() {
        sleep(4)

        let workoutsStaticText = app.staticTexts["Workouts"]
        workoutsStaticText.swipeLeft()
        
        XCTAssert(app.navigationBars["My Teams"].staticTexts["My Teams"].exists)
    }
    
    func testSwipeRight() {
        sleep(4)
        
        let workoutsStaticText = app.staticTexts["Workouts"]
        workoutsStaticText.swipeRight()
        
        XCTAssert(app.navigationBars["Coached Teams"].staticTexts["Coached Teams"].exists)
    }
    
    func testOpenWorkout() {
        sleep(4)
        
        let app = XCUIApplication()
        app.tables.staticTexts["still not leg day"].tap()
        XCTAssert(app.navigationBars["still not leg day"].exists)
    }
}
