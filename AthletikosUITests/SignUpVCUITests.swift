//
//  SignUpVCUITests.swift
//  Athletikos
//
//  Created by Daniel Porter on 4/18/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

//User should not be logged in

import XCTest

class SignUpVCUITests: XCTestCase {
    let app = XCUIApplication()
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCancelButton() {
        app.buttons["Sign-Up"].tap()
        app.buttons["Cancel"].tap()
        XCTAssert(app.buttons["Sign-Up"].exists)
    }
    
    func testSuccessfulSignUp() {
        app.buttons["Sign-Up"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("throwAwayPingoBango@gmail.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("password")
        app.buttons["Sign Up"].tap()
        
        sleep(4)
        XCTAssert(app.buttons["Sign-In"].exists)
    }
}
