//
//  LoginVCUITests.swift
//  Athletikos
//
//  Created by Daniel Porter on 4/18/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

//For these tests, make sure that you are signed out of the app

import XCTest

class LoginVCUITests: XCTestCase {
    let app = XCUIApplication()
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSuccessfulLogin() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("natechenry111@gmail.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("password")
        app.buttons["Sign-In"].tap()
        
        sleep(4)
        XCTAssert(app.navigationBars["My Page"].staticTexts["My Page"].exists)
    }
    
    func testUnsuccessfulLogin() {
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("natechenry111@gmail.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("passord")
        app.buttons["Sign-In"].tap()
        
        sleep(4)
        XCTAssert(!app.navigationBars["My Page"].staticTexts["My Page"].exists)
    }
    
    func testPressingSignUpButton() {
        app.buttons["Sign-Up"].tap()
        XCTAssert(app.buttons["Sign Up"].exists)
    }
}
