//
//  AthletikosUITests.swift
//  AthletikosUITests
//
//  Created by Andrew Henry on 1/31/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class AthletikosUITests: XCTestCase {
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //Swiping Testing
    
    func test_personal_page_left_swipe() {
        //Swipes to the my teams page
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.navigationBars["Subscribed Teams"].exists)
    }
    
    func test_my_teams_page_left_swipe() {
        //Swipes to the coached teams page
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.otherElements.containing(.navigationBar, identifier:"Subscribed Teams").children(matching: .other).element.swipeLeft()
        
        XCTAssert(app.staticTexts["Coached Teams"].exists)
        
    }
    
    func test_coached_teams_page_left_swipe() {
        //Swipes to the personal page
        app.staticTexts["Start Workout"].swipeRight()
        
        app.otherElements.containing(.navigationBar, identifier:"Coached Teams").children(matching: .other).element.swipeLeft()
        
        XCTAssert(app.staticTexts["Personal"].exists)
    }
    
    func test_personal_page_right_swipe() {
        //Swipes to the coached teams page
        app.staticTexts["Start Workout"].swipeRight()
        
        XCTAssert(app.staticTexts["Coached Teams"].exists)
    }
    
    func test_my_teams_page_right_swipe() {
        //Swipes to the personal page
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.otherElements.containing(.navigationBar, identifier:"Subscribed Teams").children(matching: .other).element.children(matching: .other).element.swipeRight()
        
        XCTAssert(app.staticTexts["Personal"].exists)
    }
    
    func test_coached_teams_page_right_swipe() {
        //Swipes to the my teams page
        //Swipes to the coached teams page
        app.staticTexts["Start Workout"].swipeRight()
        
        //Swipes to the coached teams page
        app.otherElements.containing(.navigationBar, identifier:"Coached Teams").children(matching: .other).element.children(matching: .other).element.swipeRight()
        
        
        XCTAssert(app.staticTexts["Subscribed Teams"].exists)
    }
    
    
    //Carousel indicator
    func test_carousel_indicator_personal_page() {
        //Did the carousel indicator load?
        XCTAssert(app.pageIndicators.count == 1)
    }
    
    func test_carousel_indicator_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.staticTexts["Subscribed Teams"].exists)
        XCTAssert(app.pageIndicators.count == 1)
    }
    
    func test_carousel_indicator_coached_teams_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        XCTAssert(app.staticTexts["Coached Teams"].exists)
        XCTAssert(app.pageIndicators.count == 1)
    }
    
    
    
    //Access settings
    func test_access_settings_page_from_personal_page() {
        XCUIApplication().navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.staticTexts["Settings"].exists)
    }
    
    func test_access_settings_page_from_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCUIApplication().navigationBars["Subscribed Teams"].buttons["⚙"].tap()
        
        XCTAssert(app.staticTexts["Settings"].exists)
    }
    
    func test_access_settings_page_from_coached_teams_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        XCUIApplication().navigationBars["Coached Teams"].buttons["⚙"].tap()
        
        XCTAssert(app.staticTexts["Settings"].exists)
    }
    
    
    //Return from settings
    func test_return_from_settings_page_to_personal_page() {
        XCUIApplication().navigationBars["Personal"].buttons["⚙"].tap()
        
        XCUIApplication().navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Personal"].exists)
    }
    
    func test_return_from_settings_page_to_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCUIApplication().navigationBars["Subscribed Teams"].buttons["⚙"].tap()
        
        XCUIApplication().navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Subscribed Teams"].exists)
    }
    
    func test_return_from_settings_page_to_coached_teams_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        XCUIApplication().navigationBars["Coached Teams"].buttons["⚙"].tap()
        
        XCUIApplication().navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Coached Teams"].exists)
        
        XCUIApplication().tables["Empty list"].tap()
        
    }
    
    
    
    //Start workout
    func test_start_workout_from_personal_page() {
        XCTAssert(app.staticTexts["Personal"].exists)
        
        app.tables.cells.staticTexts["Lifting Workout"].tap()
        
        XCTAssert(app.staticTexts["Lifting Workout"].exists)
    }
    
    func test_start_workout_from_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.cells.staticTexts["Training Team"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Training Team"].exists)
        
        app.tables.cells.staticTexts["Lifting Workout"].tap()
        
        XCTAssert(app.staticTexts["Lifting Workout"].exists)
    }
    
    
    //Loading workout information
    func test_workout_information_loading() {
        XCTAssert(app.staticTexts["Personal"].exists)
        
        app.tables.cells.staticTexts["Lifting Workout"].tap()
        
        XCTAssert(app.staticTexts["Lifting Workout"].exists)
        
        XCTAssert(app.tables.cells.count > 0)
    }
    
    
    //Goals loading - Personal Page
    //Most of these tests just rely on the app not crashing
    
    //Need to figure out how to change what loads and what doesn't load here
    func test_left_swipe_goals_collection_to_load_more_goals_personal_page() {
        XCTAssert(app.navigationBars.staticTexts["Personal"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_left_swipe_of_goals_with_no_goals_to_load_personal_page() {
        XCTAssert(app.navigationBars.staticTexts["Personal"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_right_swipe_goals_collection_to_load_more_goals_personal_page() {
        XCTAssert(app.navigationBars.staticTexts["Personal"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    func test_right_swipe_of_goals_with_no_goals_to_load_personal_page() {
        XCTAssert(app.navigationBars.staticTexts["Personal"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    //What is the proper goal information?
    func test_proper_goal_information_loaded_personal_page() {
        XCTAssert(app.navigationBars["Personal"].exists)
        XCTAssert(app.collectionViews.count == 1)
        XCTAssert(app.collectionViews.element(boundBy: 0).cells["Lift Goal"].exists)
    }
    
    //Goals loading - team page
    func test_left_swipe_goals_collection_to_load_more_goals_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.cells.staticTexts["Training Team"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Training Team"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_left_swipe_of_goals_with_no_goals_to_load_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.cells.staticTexts["Training Team"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Training Team"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_right_swipe_goals_collection_to_load_more_goals_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.cells.staticTexts["Training Team"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Training Team"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    func test_right_swipe_of_goals_with_no_goals_to_load_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.cells.staticTexts["Training Team"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Training Team"].exists)
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    
    func test_proper_goal_information_loaded_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.navigationBars["Subscribed Teams"].exists)
        
        XCTAssert(app.collectionViews.count == 1)
        
        XCTAssert(app.collectionViews.element(boundBy: 0).cells["Lift Goal"].exists)
    }
    
    //Goals loading - coached team page
    func test_left_swipe_goals_collection_to_load_more_goals_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.cells.staticTexts["My Team"].tap()
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_left_swipe_of_goals_with_no_goals_to_load_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.cells.staticTexts["My Team"].tap()
        
        app.collectionViews.element(boundBy: 0).swipeLeft()
    }
    
    func test_right_swipe_goals_collection_to_load_more_goals_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.cells.staticTexts["My Team"].tap()
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    func test_right_swipe_of_goals_with_no_goals_to_load_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.cells.staticTexts["My Team"].tap()
        
        app.collectionViews.element(boundBy: 0).swipeRight()
    }
    
    
    func test_proper_goal_information_loaded_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.cells.staticTexts["My Team"].tap()
            
        XCTAssert(app.collectionViews.count == 1)
            
        XCTAssert(app.collectionViews.element(boundBy: 0).cells["Lift Goal"].exists)
    }
    
    
    //Main pages graph time-resolution
    func test_statics_display_time_resolution_change_on_tap_personal_page() {
        XCTAssert(app.buttons["1-Day"].exists)
        XCTAssert(app.buttons["1-Week"].exists)
        XCTAssert(app.buttons["1-Month"].exists)

        XCTAssert(app.otherElements.matching(identifier: "PersonalChart").element.exists)
        
    }
    
    func test_statics_display_time_resolution_change_on_tap_my_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.otherElements.matching(identifier: "PersonalChart").element.exists)
        
        XCTAssert(app.buttons["1-Day"].exists)
        XCTAssert(app.buttons["1-Week"].exists)
        XCTAssert(app.buttons["1-Month"].exists)
    }
    
    func test_statics_display_time_resolution_change_on_tap_coached_team_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.otherElements.matching(identifier: "PersonalChart").element.exists)
        
        XCTAssert(app.buttons["1-Day"].exists)
        XCTAssert(app.buttons["1-Week"].exists)
        XCTAssert(app.buttons["1-Month"].exists)
    }
    
    
    //Coached teams table view testing
    func test_if_team_list_data_loaded_correctly_coached_teams_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        XCTAssert(app.navigationBars.staticTexts["Coached Teams"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells.count > 0)
    }
    
    func test_tapping_team_cell_to_load_team_page_coached_teams_page() {
        app.staticTexts["Start Workout"].swipeRight()
        
        XCTAssert(app.navigationBars.staticTexts["Coached Teams"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        //This needs to be changed
        XCTAssert(app.navigationBars.staticTexts["Hello"].exists)
    }
    
    
    //My teams table view testing
    func test_if_team_list_data_loaded_correctly_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.navigationBars.staticTexts["Subscribed Teams"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells.count > 0)
    }
    
    func test_tapping_team_cell_to_load_team_page_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.navigationBars.staticTexts["Subscribed Teams"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        //This needs to be changed
        XCTAssert(app.navigationBars.staticTexts["Hello"].exists)
    }
    
    
    //Join teams testing
    func test_overlay_popup_when_join_team_button_pressed_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.buttons["JOIN A TEAM"].tap()
        
        XCTAssert(app.alerts["Join"].exists)
    }
    
    func test_entering_valid_team_code_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        let numberOfTeams = app.tables.element(boundBy: 0).cells.count
        
        app.buttons["JOIN A TEAM"].tap()
        
        XCTAssert(app.alerts["Join"].exists)
        
        app.alerts["Join"].textFields["Type Here"].typeText("ValidCodeHere")
        app.alerts["Join"].buttons["Join"].tap()
        
        //Need to make this more obvious
        XCTAssert(app.tables.element(boundBy: 0).cells.count == (numberOfTeams + 1))
    }
    
    func test_entering_invalid_team_code_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        let numberOfTeams = app.tables.element(boundBy: 0).cells.count
        
        app.buttons["JOIN A TEAM"].tap()
        
        XCTAssert(app.alerts["Join"].exists)
        
        app.alerts["Join"].textFields["Type Here"].typeText("InvalidCodeHere")
        app.alerts["Join"].buttons["Join"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells.count == numberOfTeams)
    }
    
    func test_canceling_join_team_overlay_my_teams_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        let numberOfTeams = app.tables.element(boundBy: 0).cells.count
        
        app.buttons["JOIN A TEAM"].tap()
        
        XCTAssert(app.alerts["Join"].exists)
        
        app.alerts["Join"].buttons["Cancel"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells.count == numberOfTeams)
    }
    
    
    //Specific my team page
    func test_back_button_to_return_to_my_teams_page_from_specific_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        app.navigationBars.buttons["Back"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Subscribed Teams"].exists)
    }
    
    func test_tapping_on_workout_list_item_view_upcoming_workout_specific_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
    }
    
    //Specific coached team page
    func test_next_workout_list_loaded_with_correct_information_specific_coached_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.tables["Next Workout"].cells["Lifting Workout"].exists)
    }
    
    func test_tapping_an_upcoming_workout_list_item_specific_coached_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.tables["Upcoming Workout"].cells["Lifting Workout"].exists)
        
        app.tables["Upcoming Workout"].cells["Lifting Workout"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout"].exists)
    }
    
    func test_completed_workouts_loads_the_correct_information_specific_coached_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.tables["Next Workout"].cells["Lifting Workout"].exists)
        
        app.tables["Upcoming Workout"].cells["Lifting Workout"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Rob"].exists)
    }
    
    func test_tapping_a_completed_workout_list_item_specific_coached_team_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.tables["Completed Workout"].cells["Lifting Workout Done"].exists)
        
        app.tables["Completed Workout"].cells["Lifting Workout Done"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout Done"].exists)
    }


    //Completed team workout page tests
    func test_team_members_and_workout_completion_loaded_correctly_team_workout_results_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.tables["Next Workout"].cells["Lifting Workout"].exists)
        
        app.tables["Upcoming Workout"].cells["Lifting Workout"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Rob"].exists)
    }

    func test_team_member_list_item_tap_team_workout_results_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.tables["Next Workout"].cells["Lifting Workout"].exists)
        
        app.tables["Upcoming Workout"].cells["Lifting Workout"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Rob"].exists)
        
        app.tables.element(boundBy: 0).cells["Rob"].tap()
        
        XCTAssert(app.navigationBars["Rob"].exists)
    }

    func test_back_button_tap_to_return_to_coached_team_page_team_workout_results_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        XCTAssert(app.tables["Next Workout"].cells["Lifting Workout"].exists)
        
        app.tables["Upcoming Workout"].cells["Lifting Workout"].tap()
        
        XCTAssert(app.navigationBars["Lifting Workout"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Rob"].exists)
        
        app.tables.element(boundBy: 0).cells["Rob"].tap()
        
        XCTAssert(app.navigationBars["Rob"].exists)
        
        app.navigationBars["Rob"].buttons["Back"].tap()
        
        app.navigationBars["Lifting Workout"].buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Coached Teams"].exists)
    }


    //Exercise completed page tests
    func test_exercise_information_loaded_correctly_exercise_completed_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables["Subscribed Teams"].cells.element(boundBy: 0).tap()
        
        app.tables["Next Workouts"].cells.element(boundBy: 0).tap()
        
        app.buttons["Complete Workout"].tap()
        
        XCTAssert(app.navigationBars["Workout Completed"].exists)
    }

    func test_back_button_tap_returns_to_completed_team_workout_page_exercise_completed_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.tables["Subscribed Teams"].cells.element(boundBy: 0).tap()
        
        app.tables["Next Workouts"].cells.element(boundBy: 0).tap()
        
        app.buttons["Complete Workout"].tap()
        
        XCTAssert(app.navigationBars["Workout Completed"].exists)
        
        app.navigationBars["Back"].tap()
        
        XCTAssert(app.navigationBars["Lifting Team"].exists)
    }

    //Goals page tests
    func test_goal_page_loaded_correctly_goal_page() {
        app.collectionViews.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars.staticTexts["Goals"].exists)
    }
    
    func test_back_button_tap_returns_to_personal_page_goal_page() {
        app.collectionViews.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars.staticTexts["Goals"].exists)
        
        app.navigationBars.buttons["Back"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["Personal"].exists)
    }
    
    func test_tapping_goal_brings_up_goal_goal_page() {
        app.collectionViews.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars.staticTexts["Goals"].exists)
        
        app.collectionViews.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars.staticTexts["Goal"].exists)
    }
    
    func test_tapping_add_goal_button_opens_goal_submission_overlay_goal_page() {
        app.collectionViews.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars["Goals"].staticTexts["Goals"].exists)
        
        app.navigationBars.buttons["+"].tap()
        
        XCTAssert(app.alerts["Add Goal"].exists)
    }



    //In-progress workout page tests
    func test_tapping_reset_button_pauses_the_timer_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.buttons["⏸"].tap()
        
        XCTAssert(app.tables["/(timeElapsed)"].staticTexts["/(timeElapsed)"].exists)
    }

    func test_tapping_resume_button_presses_the_timer_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.buttons["⏸"].tap()
        
        XCTAssert(app.tables["/(timeElapsed)"].staticTexts["/(timeElapsed)"].exists)
        
        app.buttons["⏸"].tap()
        
        XCTAssert(app.tables["/(timeElapsed)"].staticTexts["/(timeElapsed)"].exists)
    }

    func test_exercise_table_view_loads_correctly_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells.count > 1)
    }

    func test_force_touching_an_exercise_cell_opens_exercise_details_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        //This needs to change once forcetap is implemented
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).press(forDuration: 0.8);
    }

    func test_back_button_press_returns_to_personal_page_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.navigationBars["Lifting Exercise"].buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Personal"].exists)
    }

    func test_last_attempt_information_loads_correctly_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        //Change once fully implemented
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts["Last Attempt"].exists)
    }

    func test_repition_dial_changes_repition_amount_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        //Figure out how the dial is going to work
        
        let pre = app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Repetition"].staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Repetition"].swipeUp()
        
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Repetition"].staticTexts.element(boundBy: 0).label != pre)
    }

    func test_weight_dial_changes_weight_amount_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        //Figure out how the dial is going to work
        
        let pre = app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Weight"].staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Weight"].swipeUp()
        
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Weight"].staticTexts.element(boundBy: 0).label != pre)
    }

    func test_partial_right_swipe_of_an_exercise_list_item_reveals_a_green_check_mark_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).swipeRight()
        
        //TODO:
        //Change this once the swiping is implemented
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Check"].exists)
    }

    func test_right_swiping_exercise_list_item_indicates_the_exercise_is_complete_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).swipeRight()
        
        //TODO:
        //Change this once the swiping is implemented
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["Check"].exists)
    }

    func test_partial_left_swiping_an_exercise_list_item_reveals_red_x_mark_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).swipeLeft()
        
        //TODO:
        //Change this once the swiping is implemented
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["X"].exists)
    }

    func test_left_swiping_exercise_list_item_indicates_exercise_is_skipped_in_progress_workout_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).swipeLeft()
        
        //TODO:
        //Change this once the swiping is implemented
        XCTAssert(app.tables.element(boundBy: 0).cells.element(boundBy: 0).otherElements["X"].exists)
    }
    
    
    //Exercise details page tests
    func test_image_progression_loads_correctly_exercise_details_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        let exerciseName = app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars[exerciseName].exists)
        
        XCTAssert(app.images.element(boundBy: 0).exists)
    }

    func test_repetition_dial_changes_repetition_amount_exercise_details_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        let exerciseName = app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars[exerciseName].exists)
        
        let pre = app.otherElements["Repetition"].staticTexts.element(boundBy: 0).label
        
        app.tables.otherElements["Repetition"].swipeUp()
        
        XCTAssert(app.otherElements["Repetition"].staticTexts.element(boundBy: 0).label != pre)
    }

    func test_weight_dial_changes_weight_amount_exercise_details_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        let exerciseName = app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars[exerciseName].exists)
        
        let pre = app.otherElements["Weight"].staticTexts.element(boundBy: 0).label
        
        app.otherElements["Weight"].swipeUp()
        
        XCTAssert(app.otherElements["Weight"].staticTexts.element(boundBy: 0).label != pre)
    }

    func test_exercise_description_loads_correctly_exercise_details_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        let exerciseName = app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars[exerciseName].exists)
        
        //Change this once it is implemented to be the actual thing
        XCTAssert(app.staticTexts.element(boundBy: 3).exists)
    }

    func test_exercise_history_stats_display_loads_correctly_exercise_details_page() {
        app.tables.element(boundBy: 0).cells["Lifting Exercise"].tap()
        
        XCTAssert(app.navigationBars["Lifting Exercise"].exists)
        
        let exerciseName = app.tables.element(boundBy: 0).cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        XCTAssert(app.navigationBars[exerciseName].exists)
        
        //Change this once it is implemented to be the actual thing
        XCTAssert(app.staticTexts.element(boundBy: 3).exists)
    }



    //Settings page tests
    func test_back_button_tap_returns_to_personal_page_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        app.navigationBars["Settings"].buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Personal"].exists)
    }
    
    func test_back_button_tap_returns_to_my_teams_page_settings_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.navigationBars["Subscribed Teams"].buttons["⚙"].tap()
        
        app.navigationBars["Settings"].buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Subscribed Teams"].exists)
    }
    
    func test_back_button_tap_returns_to_coached_teams_page_settings_page() {
        app.staticTexts["Start Workout"].swipeLeft()
        
        app.navigationBars["Coached Teams"].buttons["⚙"].tap()
        
        app.navigationBars["Settings"].buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Coached Teams"].exists)
    }
    
    func test_page_information_is_loaded_correctly_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells.count == 6)
        XCTAssert(app.tables.element(boundBy: 0).cells["Subscribed Teams"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells["Coached Teams"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells["Personal"].exists)
        
        XCTAssert(app.tables.element(boundBy: 0).cells["All Notifications"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells["Personal Notifications"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells["Team Notifications"].exists)
        XCTAssert(app.tables.element(boundBy: 0).cells["No Notifications"].exists)
    }
    
    func test_personal_starting_page_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["Personal"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Personal"].images["check.png"].exists)
    }
    
    func test_my_teams_starting_page_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["Subscribed Teams"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Subscribed Teams"].images["check.png"].exists)
    }
    
    func test_coached_teams_starting_page_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["Coached Teams"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Coached Teams"].images["check.png"].exists)
    }
    
    func test_all_notifications_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["All Notifications"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["All Notifications"].images["check.png"].exists)
    }
    
    func test_team_notification_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["Team Notifications"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Team Notifications"].images["check.png"].exists)
    }
    
    func test_personal_notification_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["Personal Notifications"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["Personal Notifications"].images["check.png"].exists)
    }
    
    func test_no_notifications_tap_settings_page() {
        app.navigationBars["Personal"].buttons["⚙"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
        
        app.tables.element(boundBy: 0).cells["No Notifications"].tap()
        
        XCTAssert(app.tables.element(boundBy: 0).cells["No Notifications"].images["check.png"].exists)
    }
}
