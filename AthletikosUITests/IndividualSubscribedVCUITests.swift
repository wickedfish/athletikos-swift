//
//  IndividualSubscribedVCUITests.swift
//  Athletikos
//
//  Created by Zach on 4/19/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class IndividualSubscribedVCUITests: XCTestCase {
        let app = XCUIApplication()
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAllElementsExist() {
        sleep(4)
        XCTAssert(app.tables.element(boundBy:0).exists)
        XCTAssert(app.collectionViews.element(boundBy: 0).exists)
        XCTAssert(XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.exists)
        
    }
    func testtesttest(){
        
        let app = XCUIApplication()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.swipeLeft()
        
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Wicked Fish"].tap()
        tablesQuery.staticTexts["still not leg day"].tap()
        
        XCTAssert(app.buttons["Workout Preview"].exists)
        
        
    }
    
    func testChartSwitch(){
        
        let app = XCUIApplication()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).swipeLeft()
        app.tables.staticTexts["Wicked Fish"].tap()
        app.buttons["Week"].tap()
        XCTAssert(app.buttons["Week"].isSelected)
        
    }
    func testChartSwitch2(){
        
        let app = XCUIApplication()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).swipeLeft()
        app.tables.staticTexts["Wicked Fish"].tap()
        app.buttons["Month"].tap()
        XCTAssert(app.buttons["Month"].isSelected)
        
    }
    func testBack(){
        
        let app = XCUIApplication()
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1)
        element.swipeLeft()
        app.tables.staticTexts["Wicked Fish"].tap()
        app.navigationBars["Wicked Fish"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        XCTAssert(app.buttons["JOIN A TEAM"].exists)
        
    }
    
}
