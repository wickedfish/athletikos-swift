//
//  UITestsSettingsPageVC.swift
//  Athletikos
//
//  Created by Nate Henry on 4/18/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class UITestsSettingsPageVC: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBackButton() {
        let app = XCUIApplication()
        
        app.navigationBars["Personal"].buttons["⚙"].tap()
        app.navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        XCTAssert(app.staticTexts["Personal"].exists)
    }
    
    func testEditProfileButton() {
        let app = XCUIApplication()
        
        app.navigationBars["Personal"].buttons["⚙"].tap()
        XCUIApplication().tables.buttons["Edit Profile"].tap()
        
        XCTAssert(app.buttons["Password Reset"].exists)
    }
    
    func testSignOut(){
        let app = XCUIApplication()
        
        app.navigationBars["Personal"].buttons["⚙"].tap()
        app.tables.buttons["Sign Out"].tap()
        app.alerts["Sign-Out"].buttons["Yes"].tap()
        
        
        XCTAssert(app.buttons["Sign-In"].exists)
    }
    
    func testEditProfileBackButton(){
        let app = XCUIApplication()
        
        app.navigationBars["Personal"].buttons["⚙"].tap()
        XCUIApplication().tables.buttons["Edit Profile"].tap()
        XCUIApplication().buttons["Back"].tap()
        
        XCTAssert(app.navigationBars["Settings"].exists)
    }
}
