//
//  UITestSubscribedTeamsVC.swift
//  Athletikos
//
//  Created by Nate Henry on 4/19/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import XCTest

class UITestSubscribedTeamsVC: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTeamSelection(){
        let app = XCUIApplication()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1)
        element.swipeRight()
        element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.swipeRight()
        app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()
        
        
        XCTAssert(app.staticTexts["Goals"].exists)
    }
    
    func testJoinTeamButton(){
        let app = XCUIApplication()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1)
        element.swipeRight()
        element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.swipeRight()
        
        app.buttons["JOIN A TEAM"].tap()
        XCTAssert(app.alerts["Join"].staticTexts["Join"].exists)
        
    }
    
    func testJoinTeamCancelButton(){
        let app = XCUIApplication()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1)
        element.swipeRight()
        element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.swipeRight()
        
        app.buttons["JOIN A TEAM"].tap()
        app.alerts["Join"].buttons["Cancel"].tap()
        
        
        XCTAssert(app.navigationBars["Subscribed Teams"].staticTexts["Subscribed Teams"].exists)
    }
}
