//
//  StartTableRow.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/23/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import WatchKit

class WorkoutTableRow: NSObject {
    @IBOutlet var RowBackground: WKInterfaceGroup!
    @IBOutlet var WorkoutName: WKInterfaceLabel!
    @IBOutlet var RowSeparator: WKInterfaceSeparator!
}
