//
//  PStartIC.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/23/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import WatchKit
import Foundation


class PStartIC: WKInterfaceController {
    @IBOutlet var startWorkoutButton: WKInterfaceButton!

    @IBOutlet var exerciseTable: WKInterfaceTable!

    override func awake(withContext context: Any?) {
        self.setTitle("WORKOUT NAME HERE")
        super.awake(withContext: context)
        exerciseTable.setNumberOfRows(3, withRowType: "ExerciseTR")
        
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
