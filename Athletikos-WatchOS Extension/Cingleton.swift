//
//  Cingleton.swift
//  Athletikos
//
//  Created by Daniel Porter on 3/9/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import Foundation
import WatchConnectivity

class Cingleton: NSObject, WCSessionDelegate {
    let session = WCSession.default()
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func configure(){
        session.delegate = self
        session.activate()
    }
    
    func sendData(f: @escaping () -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            f()
        }
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        sendData {
            self.processApplicationContext()
        }
    }
    
    func processApplicationContext() {
        if let iPhoneContext = session.receivedApplicationContext as? [String: Bool] {
            if iPhoneContext["switchStatus"] == true {
                
            } else {
                
            }
        }
    }
    
    //Make this a Singleton class
    static let sharedInstance: Cingleton = {
        let instance = Cingleton()
        return instance
    }()
}
