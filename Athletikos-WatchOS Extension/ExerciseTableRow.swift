//
//  ExerciseTableRow.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/23/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import WatchKit

class ExerciseTableRow: NSObject {
    
    @IBOutlet var exerciseDetailsLabel: WKInterfaceLabel!
    @IBOutlet var exerciseNameLabel: WKInterfaceLabel!
}
