//
//  ExerciseEditIC.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/23/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import WatchKit
import Foundation


class ExerciseEditIC: WKInterfaceController {
    var numberList: [Int] = [
        (1),
        (2),
        (3),
        (4),
        (5),
        (6) ]
    
    @IBOutlet var rightEditPicker: WKInterfacePicker!

    @IBOutlet var leftEditPicker: WKInterfacePicker!

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let pickerItemsReps: [WKPickerItem] = numberList.map {
            let pickerItem = WKPickerItem()
            pickerItem.title = String($0)
            pickerItem.caption = "Reps"
            return pickerItem
        }
        
        let pickerItemsWeight: [WKPickerItem] = numberList.map {
            let pickerItem = WKPickerItem()
            pickerItem.title = String($0)
            pickerItem.caption = "Weight"
            return pickerItem
        }
        
        rightEditPicker.setItems(pickerItemsReps)
        leftEditPicker.setItems(pickerItemsWeight)
        
        // Configure interface objects here.

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
