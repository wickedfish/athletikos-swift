//
//  StartIC.swift
//  Athletikos
//
//  Created by Daniel Porter on 2/21/17.
//  Copyright © 2017 Andrew Henry. All rights reserved.
//

import WatchKit
import Foundation

class StartIC: WKInterfaceController {
    @IBOutlet var WorkoutTable: WKInterfaceTable!
    
    let cing = Cingleton.sharedInstance

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        WorkoutTable.setNumberOfRows(10, withRowType: "WorkoutTR")
        
        cing.configure()
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
